<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sponsor extends Model
{
    public function categories()
    {
        return $this->belongsToMany('App\Category');
    }

    public function links()
    {
        return $this->hasMany('App\SponsorLinks');
    }
}
