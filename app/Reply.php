<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reply extends Model
{
    public function owner() {
        return $this->belongsTo('App\User');
    }

    public function thread() {
        return $this->belongsTo('App\Thread');
    }

    public function favourites() {
        return $this->hasMany('App\ThreadFavourites');
    }

    public function quoted() {
        return $this->hasOne('App\Reply','id', 'quoted_id');
    }

    public function likes() {
        return $this->hasMany('App\ReplyLikes', 'item_id', 'id');
    }
}
