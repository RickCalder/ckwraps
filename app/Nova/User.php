<?php

namespace App\Nova;

use App\Nova\Filters\MemberType;
use App\Nova\Filters\Found;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Gravatar;
use Laravel\Nova\Fields\Password;
use Laravel\Nova\Fields\HasOne;
use Laravel\Nova\Fields\Date;

class User extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\User';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id', 'name', 'email',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),

            Gravatar::make(),

            Text::make('Name')
                ->sortable()
                ->rules('required', 'max:255'),

            Text::make('Email')
                ->sortable()
                ->rules('required', 'email', 'max:254')
                ->creationRules('unique:users,email')
                ->updateRules('unique:users,email,{{resourceId}}'),

            Password::make('Password')
                ->onlyOnForms()
                ->creationRules('required', 'string', 'min:8')
                ->updateRules('nullable', 'string', 'min:8'),

            Textarea::make('Cancel Reason')->sortable(),
                
            Select::make('Role', 'role_id')->options([
                '1' => 'Admin',
                '2' => 'Free Premium Member',
                '3' => 'Member',
                '4' => 'Premium Member',
            ])->displayUsingLabels(),
                
            Select::make('Found Us', 'found')->options([
                'Social Media' => 'YouTube / Facebook Ads',
                'YouTube' => 'Found on YouTube',
                'Referral' => 'Referral',
                'Web Search' => 'Web Search',
            ])->displayUsingLabels(),

            Text::make('Address1')
                ->hideFromIndex()
                ->rules('max:255'),

            Text::make('Address2')
            ->hideFromIndex()
                ->rules('max:255'),

            Text::make('City')
            ->hideFromIndex()
                ->rules('max:255'),

            Text::make('State')
            ->hideFromIndex()
                ->rules('max:255'),

            Text::make('Country')
                ->sortable()
                ->rules('max:255'),

            Text::make('Zip')
            ->hideFromIndex()
                ->rules('max:255'),

            Date::make('PayPal End Date')->hideFromIndex(),


            HasOne::make('Location', 'location')
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [
            new MemberType,
            new Found,
            new Filters\Country
        ];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
