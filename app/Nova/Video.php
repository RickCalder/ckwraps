<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Trix;
use Laravel\Nova\Fields\Select;

class Video extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Video';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id', 'title'
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),

            Text::make('Title')
                ->sortable()
                ->rules('required', 'max:255'),
            Text::make('Video ID')
                ->sortable()
                ->rules('required', 'max:255')->hideFromIndex(),
            Text::make('Private Link')
                ->sortable()
                ->rules('required', 'max:255')->hideFromIndex(),
            Number::make('Hours')->hideFromIndex(),
            Number::make('Minutes')->hideFromIndex(),
            Number::make('Seconds'),
            Select::make('Source')->options([
                'vimeo' => 'Vimeo',
                'youtube' => 'YouTube'
            ]),
            DateTime::make('Created At')->hideFromIndex()->withMeta(['value' => date("Y-m-d H:i:s")]),

            Trix::make('Description')
                ->sortable()->hideFromIndex(),
            Select::make('Category', 'category_id')->options([
                '1' => 'General',
                '2' => 'Disassembly',
                '3' => 'Installation',
                '4' => 'Advanced',
                '5' => 'Techniques',
                '6' => 'Hoods',
                '7' => 'Door Handles',
                '8' => 'Bumpers',
                '9' => 'Roofs',
                '10' => 'Mirrors',
                '15' => 'Doors',
                '11' => 'Fenders',
                '12' => 'Trunks',
                '13' => 'Headlight/Taillight Tints',
                '14' => 'Interior',
                '16' => 'Beginner-101',
                '17' => 'PPF',
                '18' => 'Business-101',
                '19' => 'Level 2 Workshop Program',
                '20' => 'Getting Started'

            ])->displayUsingLabels(),
            Image::make('Thumbnail')->deletable()->prunable()->hideFromIndex(),

        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
