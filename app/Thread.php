<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Thread extends Model
{
    use Sluggable;

    public function user() {
        return $this->belongsTo('App\User')->withTrashed();
    }

    public function tag() {
        return $this->belongsTo('App\ThreadTag');
    }

    public function replies() {
        return $this->hasMany('App\Reply');
    }

    public function likes() {
        return $this->hasMany('App\ForumLikes', 'item_id', 'id');
    }

    public static function boot() {
        parent::boot();

        static::deleting(function($thread) {
             $thread->replies()->delete();
        });
    }

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
    
}
