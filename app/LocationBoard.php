<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocationBoard extends Model
{
    public function user() {
        return $this->hasOne('App\User');
    }
}
