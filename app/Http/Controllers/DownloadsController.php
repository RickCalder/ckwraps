<?php

namespace App\Http\Controllers;

use App\Downloads;
use Illuminate\Http\Request;

class DownloadsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
        $allowed = \Request::get('userAllowed');

        $downloads = Downloads::all();
        
        return view('downloads', ['downloads' => $downloads, 'allowed' => $allowed]);
        
    }
}
