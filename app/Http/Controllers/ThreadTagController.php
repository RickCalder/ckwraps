<?php

namespace App\Http\Controllers;

use App\ThreadTag;
use App\Thread;
use \DB;
use App\Http\Controllers\Controller;

class ThreadTagController extends Controller
{
    public function show($tag)
    {
        $tags = ThreadTag::orderBy('tag_name')->get();
        $tagId = ThreadTag::where('tag_slug', $tag)->first();

        $allowed = '0';
        $allowed = \Request::get('userAllowed');

        $threads = Thread::with('replies', 'replies.owner', 'tag', 'user')
        ->where('announcement', '=', 0)
            ->where('threads.tag_id', $tagId->id)
            ->orderBy('updated_at', 'DESC')
            ->paginate(25);

            
        $announcements = Thread::with('replies', 'replies.owner', 'tag', 'user')
        ->where('announcement', '>=', 1)
        ->orderBy('updated_at', 'DESC')
        ->paginate(25);

        return view('forums.overview', ['searchTerm' =>'','threads' => $threads, 'activeTag' => $tag, 'tags' => $tags, 'allowed' => $allowed, 'current_tag' => $tagId,'announcements'=> $announcements]);
    }
}
