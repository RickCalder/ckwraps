<?php

namespace App\Http\Controllers;

use App\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */

   public function index(Request $request)
   {
     $subject = $request->subject;
     $subject = str_replace('-', ' ', $subject);
 
     return view('contact', ['subject'=> $subject]);
   }

   public function contact_ppf(Request $request)
   {
     return view('contact-ppf');
   }
   
   public function contact_cppf(Request $request)
   {
     return view('contact-cppf');
   }

  public function store(Request $request)
  {
    $v = \Validator::make($request->all(), [
      'name' => 'required',
      'subject' => 'required',
      'email' => 'required',
      'message' => 'required',
      'g-recaptcha-response' => 'required|recaptcha'
    ]);

    if ($v->fails()) {
      return redirect()->back()->withInput()->withErrors($v);
    }
    $contact = new Contact;
    $contact->name = $request['name'];
    $contact->email = $request['email'];
    $contact->subject = $request['subject'];
    $contact->message = $request['message'];

    $contact->save();
    // dd(config('app.mail_to'));
    try {
      \Mail::send(
        'emails.contactform',
        array(
          'name' => $request->get('name'),
          'email' => $request->get('email'),
          'subject' => $request->get('subject'),
          'phone' => $request->get('phone'),
          'user_message' => $request->get('message')
        ),
        function ($message) use ($request) {
          // $ccEmails = ['nav@ckwraps.com', 'rick@calder.io']
          $message->from('nav@ckwraps.com');
          $message->to('nav@ckwraps.com', 'CKWraps Website')
          // $message->cc($ccEmails)
          // $message->to('rick@calder.io');
          // $message->cc('calder12@gmail.com'),
          ->replyTo($request->get('email'))
          ->subject($request->get('subject'));
        }
      );
    } catch (\Exception $e) {

      // dd($e);
      return redirect()->back()->with(['status' => 'There is a problem with sending this message, please try again later or you can email info@ckwraps.com directly. We apologize for the inconvenience']);
    }

    return redirect()->back()->with(['status' => 'Thank you, your message was sent, we\'ll get in touch soon!']);
  }

  
  public function mail_ppf(Request $request)
  {
    // dd($request->all());
    try {
      \Mail::send(
        'emails.contactform-ppf',
        array(
          'name' => $request->get('name'),
          'email' => $request->get('email'),
          'subject' => "PPF Certification registration",
          'phone' => $request->get('phone'),
          'website' => $request->get('website'),
          'applicants' => $request->get('applicants'),
          'address' => $request->get('address'),
          'user_message' => $request->get('profile'),
          'hear' => $request->get('hear'),
          'business' => $request->get('business')
        ),
        function ($message) use ($request) {
          // $ccEmails = ['nav@ckwraps.com', 'rick@calder.io']
          $message->from('nav@ckwraps.com');
          $message->to('nav@ckwraps.com', 'CKWraps Website')
          // $message->cc($ccEmails)
          // $message->to('rick@calder.io')
          ->replyTo($request->get('email'))
          ->subject($request->get('subject'));
        }
      );
    } catch (\Exception $e) {
      return redirect()->back()->with(['status' => 'There is a problem with sending this message, please try again later or you can email info@ckwraps.com directly. We apologize for the inconvenience']);
    }

    return redirect()->back()->with(['status' => 'Thank you, your message was sent, we\'ll get in touch soon!']);
  }
  
  public function mail_cppf(Request $request)
  {
    // dd($request->all());
    try {
      \Mail::send(
        'emails.contactform-ppf',
        array(
          'name' => $request->get('name'),
          'email' => $request->get('email'),
          'subject' => "Cosmetic PPF Certification registration",
          'phone' => $request->get('phone'),
          'website' => $request->get('website'),
          'applicants' => $request->get('applicants'),
          'address' => $request->get('address'),
          'user_message' => $request->get('profile'),
          'hear' => $request->get('hear'),
          'business' => $request->get('business')
        ),
        function ($message) use ($request) {
          // $ccEmails = ['nav@ckwraps.com', 'rick@calder.io']
          $message->from('nav@ckwraps.com');
          $message->to('nav@ckwraps.com', 'CKWraps Website')
          // $message->cc($ccEmails)
          // $message->to('rick@calder.io')
          ->replyTo($request->get('email'))
          ->subject($request->get('subject'));
        }
      );
    } catch (\Exception $e) {
      return redirect()->back()->with(['status' => 'There is a problem with sending this message, please try again later or you can email info@ckwraps.com directly. We apologize for the inconvenience']);
    }

    return redirect()->back()->with(['status' => 'Thank you, your message was sent, we\'ll get in touch soon!']);
  }
}
