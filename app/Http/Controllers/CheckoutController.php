<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mailchimp;

class CheckoutController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function subscribe_process(Request $request)
    {
        try {
            Stripe::setApiKey(env('STRIPE_SECRET'));
    
            $user = User::find(1);
            $user->newSubscription('main', 'bronze')->create($request->stripeToken);
    
            return 'Subscription successful, you get the course!';
        } catch (\Exception $ex) {
            return $ex->getMessage();
        }
    
    }
}
