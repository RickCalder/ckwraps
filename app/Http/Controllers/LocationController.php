<?php

namespace App\Http\Controllers;

use App\LocationBoard;
use Illuminate\Http\Request;

class LocationController extends Controller
{

    public function index()
    {
        $locations = LocationBoard::where('published', '=', 1)->paginate('25');
        
        return view('locations.index', ['locations' => $locations]);
    }

    public function show($id) {
        $location = LocationBoard::where('id','=',$id)->first();
        if($location === null || $location->published === 0) {
          return redirect('/locations');
        }
        return view('locations.show', ['location' => $location]);
    }

}
