<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Plan;
use Mailchimp;

class SubscriptionController extends Controller
{
    public function create(Request $request, Plan $plan)
    {
      // dd($request->all());
        $plan = Plan::findOrFail($request->get('plan'));
          $request->user()
              ->newSubscription('CkWraps Subscription', $plan->stripe_plan)
              // ->newSubscription('CKWraps Subscription', 'plan_FvlWo5r2MwLzyP')
              ->withCoupon($request->coupon)
              ->create($request->paymentMethod );

              $user = auth()->user();
              $user->role_id = 4;
              $user->address1 = $request->address1;
              $user->address2 = $request->address2;
              $user->city = $request->city;
              $user->state = $request->state;
              $user->country = $request->country;
              $user->zip = $request->zip;
              $user->save();
                      
              try {
                $user = auth()->user();
                \Mail::send(
                'emails.newsub',
                array(
                    'name' => $user->name,
                    'email' => $user->email,
                    'subject' => 'New Subscription',
                    'user_message' => $user->found
                ),
                function ($message) {
                    $message->from('nav@ckwraps.com');
                    // $message->to(config('app.mail_to', 'nav@ckwraps.com'), 'CKWraps Website');
                    $message->to('info@ckwraps.com', 'CKWraps Website')
                    // $message->to('calder12@gmail.com', 'CKWraps Website')
                    ->replyTo('info@ckwraps.com')
                    ->subject('New Subscription');
                }
                );
              } catch (\Exception $e) {
                  dd($e);
              } catch (\Stripe\Error\Base $e) {
                return redirect::back()->withErrors($e);
              }
              return redirect()->route('thankyou')->with('success', 'Your plan subscribed successfully');
            // Mailchimp::subscribe('278c64329a', $user->email, $merge = ['USERTYPE' => 'Premium Member'], $confirm = true);

        
    }

    public function cancel(Request $request) {

        // dd($request->all());
        define("SUB", 'CkWraps Subscription');
        $user = auth()->user();
        if( $user->subscribed(SUB) ) {
            $user->subscription(SUB)->cancel();
        }
        $user->role_id = 3;
        if($request->cancel_reason !== null) {
            $user->cancel_reason = $request->cancel_reason;
        }
        $user->save();        
        // try {
        //     \Mail::send(
        //       'emails.cancellation',
        //       array(
        //         'name' => $user->name,
        //         'email' => $user->email,
        //         'subject' => 'Subscription Cancellation',
        //         'user_message' => $user->cancel_reason
        //       ),
        //       function ($message) use ($request) {
        //         $message->from('nav@ckwraps.com');
        //         // $message->to(config('app.mail_to', 'nav@ckwraps.com'), 'CKWraps Website')
        //         $message->to('info@ckwraps.com', 'CKWraps Website')
        //         ->replyTo('info@ckwraps.com')
        //         ->subject('Subscription Cancellation');
        //       }
        //     );
        //   } catch (\Exception $e) {
        //     // dd($e);
            
        //     return back()->with('status', 'Your subscription has been canceled. We\'re sorry to see you go!');
        //   }
        return back()->with('status', 'Your subscription has been canceled. We\'re sorry to see you go!');
    }

    public function update_payment(Request $request) {
        $user = auth()->user();
        define("SUB", 'CkWraps Subscription');
        $user = auth()->user();
        $user->updateDefaultPaymentMethod($request->paymentMethod);
        $user->role_id = 4;
        $user->save();
        return back()->with('status', 'Your payment method has been updated!');
    }
}
