<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use App\Plan as CKPlan;

// use to process billing agreements
use PayPal\Api\Agreement;
use PayPal\Api\Payer;
use PayPal\Api\Plan;
use PayPal\Api\ShippingAddress;

use App\User;

class PaypalController extends Controller
{
    private $apiContext;
    private $mode;
    private $client_id;
    private $secret;
    private $plan_id;
    
    // Create a new instance with our paypal credentials
    public function __construct()
    {
        // Detect if we are running in live mode or sandbox
        if(config('paypal.settings.mode') == 'live'){
            $this->client_id = config('paypal.live_client_id');
            $this->secret = config('paypal.live_secret');
        } else {
            $this->client_id = config('paypal.sandbox_client_id');
            $this->secret = config('paypal.sandbox_secret');
        }
        
        // Set the Paypal API Context/Credentials
        $this->apiContext = new ApiContext(new OAuthTokenCredential($this->client_id, $this->secret));
        $this->apiContext->setConfig(config('paypal.settings'));
    }

    public function paypalRedirect($id_plan){
        // Create new agreement
        $plan_2 = CKPlan::findOrFail($id_plan);
        $agreement = new Agreement();
        $agreement->setName('CK Wraps Subscription Agreement')
          ->setDescription('Basic Subscription')
          ->setStartDate(\Carbon\Carbon::now()->addMinutes(5)->toIso8601String());

        // Set plan id
        $plan = new Plan();
        $plan->setId($plan_2->paypal_plan);
        $agreement->setPlan($plan);
        // dd($agreement);

        // Add payer type
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
        $agreement->setPayer($payer);

        try {
          // Create agreement
          $agreement = $agreement->create($this->apiContext);

          // Extract approval URL to redirect user
          $approvalUrl = $agreement->getApprovalLink();

          return redirect($approvalUrl);
        } catch (PayPal\Exception\PayPalConnectionException $ex) {
          echo $ex->getCode();
          echo $ex->getData();
          die($ex);
        } catch (Exception $ex) {
          die($ex);
        }

    }

    public function paypalReturn(Request $request){

        $token = $request->token;
        $agreement = new \PayPal\Api\Agreement();

        try {
            // Execute agreement
            $result = $agreement->execute($token, $this->apiContext);
            // dd($result);
            // $user = Auth::user();
            $user = auth()->user();
            $user->role_id = 4;
            if(isset($result->id)){
              $user->paypal_agreement_id = $result->id;
              $user->paypal_agreement_date = $result->start_date;
            }
            // $user->address1 = $request->address1;
            // $user->address2 = $request->address2;
            // $user->city = $request->city;
            // $user->state = $request->state;
            // $user->country = $request->country;
            // $user->zip = $request->zip;
            $user->save();
                    
            // try {
            //   $user = auth()->user();
            //   \Mail::send(
            //   'emails.newsub',
            //   array(
            //       'name' => $user->name,
            //       'email' => $user->email,
            //       'subject' => 'New Subscription',
            //       'user_message' => $user->found
            //   ),
            //   function ($message) {
            //       $message->from('nav@ckwraps.com');
            //       // $message->to(config('app.mail_to', 'nav@ckwraps.com'), 'CKWraps Website');
            //       $message->to('info@ckwraps.com', 'CKWraps Website')
            //       // $message->to('calder12@gmail.com', 'CKWraps Website')
            //       ->replyTo('info@ckwraps.com')
            //       ->subject('New Subscription');
            //   }
            //   );
            // } catch (\Exception $e) {
            //     dd($e);
            // } catch (\Stripe\Error\Base $e) {
            //   return redirect::back()->withErrors($e);
            // }
            return redirect()->route('thankyou')->with('success', 'Your plan subscribed successfully');
            if(isset($result->id)){
                $user->paypal_agreement_id = $result->id;
            }
            $user->save();

            echo 'New Subscriber Created and Billed';

        } catch (\PayPal\Exception\PayPalConnectionException $ex) {
            echo 'You have either cancelled the request or your session has expired';
        }
    }

    public function subscriptionCancel(Request $request) {

      $data = json_decode($request->getContent(), true);
      $user_agreement = $data['resource']['id'];
      $end_date1 = strtotime($data['resource']['agreement_details']['next_billing_date']);
      
      $end_date = date('Y-m-d', $end_date1);
      $user = User::where('paypal_agreement_id', '=', $user_agreement)->first();
      $user->paypal_end_date = $end_date;
      $user->save();
      print_r($user);
    }
}