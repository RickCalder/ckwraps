<?php

namespace App\Http\Controllers;

use App\LiveEvent;
use Illuminate\Http\Request;

class LiveEventController extends Controller
{

    public function index()
    {
        $events = LiveEvent::paginate(25);
        
        return view('liveevents.index', ['events' => $events]);
    }

}
