<?php

namespace App\Http\Controllers;

use Laravel\Cashier\Http\Controllers\WebhookController as CashierController;
use Symfony\Component\HttpFoundation\Response;
use App\User;

class StripeWebhookController extends CashierController
{
    /**
     * Handle customer subscription updated.
     *
     * @param  array  $payload
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function handleCustomerSubscriptionCreated(array $payload)
    {
        $customer = $payload['data']['object']['customer'];
        $user = User::where('stripe_id', '=', $customer)->first();
        $user->role_id = 4;
        $user->save();

        // handle the incoming event...

        return new Response('Webhook Handled', 200);
    }
}