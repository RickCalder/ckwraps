<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UploadController extends Controller
{
    public function api_sign_request($params_to_sign, $api_secret) {
        $params = array();
        foreach ($params_to_sign as $param => $value) {
            if (isset($value) && $value !== "") {
                if (!is_array($value)) {
                    $params[$param] = $value;
                } else {
                    if (count($value) > 0) {
                        $params[$param] = implode(",", $value);
                    }
                }
            }
        }
        ksort($params);
        $join_pair = function ($key, $value) {
            return $key . "=" . $value;
        };
        $to_sign = implode("&", array_map($join_pair, array_keys($params), array_values($params)));
        return sha1($to_sign . $api_secret);
    }

    public function generate_signature(Request $request) { 
        $params_to_sign = $_GET['data'];
        $api_secret = "M-SPV4enOJ3SuIS-655NZafz_-Y";

        echo $this->api_sign_request($params_to_sign, $api_secret);
    }
}