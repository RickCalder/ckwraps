<?php

namespace App\Http\Controllers;

use App\Video;
use App\User;
use App\VideoComment;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VideoController extends Controller
{

  public function categories($cat) {
    $videos = Video::where('category_id', '=', $cat)->orderBy('source', 'ASC')->orderBy('id', 'DesC')->paginate(12);
    return view('videos.index', ['videos'=>$videos, 'searchTerm' =>'', 'category' => '0']);
  }
  public function index()
  {
    $videos = Video::orderBy('id', 'desc')->orderBy('source','asc')->paginate(12);
    return view('videos.index', ['videos'=>$videos, 'searchTerm' =>'', 'category' => '0']);
  }

  public function show($id)
  {

    $allowed = false;
    if (isset(auth()->user()->role_id) && (auth()->user()->role_id == 1 || auth()->user()->role_id == 4)) {
      $allowed = true;
    } else if (auth()->user() && auth()->user()->subscribed('CkWraps Subscription') ) {
      $allowed = true;
    }
    if(\Request::get('userAllowed')) {
      $video = Video::Where('id', '=', $id)->first();
      $recommendations = Video::inRandomOrder()->where('category_id', '=', $video->category_id)->limit(5)->get();
      $comments = VideoComment::Where('video_id', '=', $video->id)->where('deleted_at', '=', null)->orderBy('created_at', 'DESC')->get();
      // dd($comments);
      return view('videos.show', ['video' => $video, 'recommendations' => $recommendations, 'comments' => $comments]);
    } 


    return redirect('videos');
  }

  public function comment(Request $request) {
    $user = auth()->user()->id;
    $comment = new VideoComment;
    $comment->comment = $request->comment;
    $comment->video_id = $request->video_id;
    if($request->quoted !== null) {
      $comment->quoted_id = $request->quoted;
      
      $reply = VideoComment::where('id', '=', $request->quoted)->first();
      $commentUser = User::where('id', '=', $reply->user_id)->first();    
      if(($commentUser->role_id === 4 || $commentUser->role_id === 1) && $commentUser->notifications === 1) {
        if($commentUser->id === auth()->user()->id)
        {
          return;
        }
        try {
          \Mail::send(
            'emails.commentreply',
            array(
              'name' => $commentUser->name,
              'email' => $commentUser->email,
              'subject' => 'New Reply to your Comment',
              'video_link' => 'https://ckwraps.com/videos/' . $request->video_id,
              'comment' => $request->comment
            ),
            function ($message) use ($request, $commentUser) {
              $message->from('nav@ckwraps.com');
              // $message->to(config('app.mail_to', 'nav@ckwraps.com'), 'CKWraps Website')
              $message->to($commentUser->email, 'CKWraps Website')
              ->replyTo('info@ckwraps.com')
              ->subject('New Reply to your Comment');
            }
          );
        } catch (\Exception $e) {
          dd($e);
        }
      }
    }
    // dd($commentUser->email);
    $comment->user_id = $user;
    $comment->save();
  
    
    // dd($request->all());
    $user = auth()->user();      
    try {
        \Mail::send(
          'emails.videocomment',
          array(
            'name' => $user->name,
            'email' => $user->email,
            'subject' => 'New Video Comment',
            'video_link' => 'https://ckwraps.com/videos/' . $request->video_id,
            'comment' => $request->comment
          ),
          function ($message) use ($request) {
            $message->from('nav@ckwraps.com');
            // $message->to(config('app.mail_to', 'nav@ckwraps.com'), 'CKWraps Website')
            $message->to('info@ckwraps.com', 'CKWraps Website')
            ->replyTo('info@ckwraps.com')
            ->subject('New Video Comment');
          }
        );
      } catch (\Exception $e) {
        dd($e);
      }
    return redirect('/videos/'.$request->video_id);
  }

  public function comment_delete($id) {
    $comment = VideoComment::Where('id', '=', $id)->first();
    $comment->deleted_at = now();
    $comment->save();
    return redirect('/videos/'.$comment->video_id);
  }

  public function search(Request $request ) {
    $q = $request->input('query');
    $cat = $request->input('category');
    // dd($q, $cat);

    if($cat === "0" && $q === null) {
      $videos = Video::orderBy('id', 'desc')->orderBy('source','asc')->paginate(12);
      return view('videos.index', ['videos'=>$videos, 'searchTerm' =>'', 'category' => $cat]);
    }
    if($cat !== "0" && $q === null) {
      $videos = Video::where('category_id', '=', $cat)
      ->orderBy('source','asc')
      ->orderBy('id', 'desc')
        ->paginate (12)->setPath ( '' );
      $pagination = $videos->appends ( array (
        'query' => $request->input('query'),
        'category' => $request->input('category')
      ) );
      // dd($videos);
      return view('videos.index', ['videos'=>$videos, 'searchTerm' => $q, 'category' => $cat])->withQuery ( $q );
    }

    if($cat !== "0" && $q !== null) {
      $videos = Video::where('category_id', '=', $cat)
      ->where( function($query) use($q) {
        $query->where ( 'title', 'LIKE', '%' . $q . '%' )
        ->orWhere ( 'description', 'LIKE', '%' . $q . '%' );
      })
      ->orderBy('source','asc')
      ->orderBy('id', 'desc')
        ->paginate (12)->setPath ( '' );
      $pagination = $videos->appends ( array (
        'query' => $request->input('query'),
        'category' => $request->input('category')
      ) );
      return view('videos.index', ['videos'=>$videos, 'searchTerm' => $q, 'category' => $cat])->withQuery ( $q );
    }

    if($cat === "0" && $q !== null) {

      $videos = Video::where ( 'title', 'LIKE', '%' . $q . '%' )
        ->orWhere ( 'description', 'LIKE', '%' . $q . '%' )
        ->orderBy('source','asc')
        ->orderBy('id', 'desc')
        ->paginate (12)->setPath ( '' );
      $pagination = $videos->appends ( array (
                  'query' => $request->input('query'),
                  'category' => $request->input('category')
          ) );
 
      return view('videos.index', ['videos'=>$videos, 'searchTerm' => $q, 'category' => $cat])->withQuery ( $q );
    }

  }
  public function comment_edit($comment_id) {
      $comment = VideoComment::where('id', '=', $comment_id)->first();
      $video = Video::where('id', '=', $comment->video_id)->first();
      if($comment->user_id === auth()->user()->id || auth()->user()->role_id <= 2) {
          return view('videos.edit_comment', ['comment' => $comment, 'video' => $video]);
      } else {
          die('nope');
      }
  }

  public function update_comment(Request $request) {
      $comment = VideoComment::where('id', '=', $request->comment_id)->first();
      $video = Video::where('id', '=', $comment->video_id)->first();

      // dd($request->all());

      if($comment->user_id === auth()->user()->id || auth()->user()->role_id <= 2) {
          $comment->comment = $request['comment'];
          $comment->save();
          return redirect('/videos/' . $video->id);
      } else {
          die('nope');
      }
  }


}
