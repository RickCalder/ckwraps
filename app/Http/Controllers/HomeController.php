<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mailchimp;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $allowed = false;
        if (isset(auth()->user()->role_id) && (auth()->user()->role_id == 1 || auth()->user()->role_id == 4)) {
          $allowed = true;
        } else if (auth()->user() && auth()->user()->subscribed('CkWraps Subscription') ) {
          $allowed = true;
        }
        return view('home', compact('allowed'));
    }
    public function training()
    {
        return view('training');
    }
    public function ppf_training()
    {
        return view('ppf_training');
    }
    public function cosmetic_ppf_training()
    {
        return view('cosmetic-ppf');
    }
    public function tos()
    {
        return view('tos');
    }
    public function privacy()
    {
        return view('privacy');
    }
    public function giveaway()
    {
        return view('giveaway');
    }


    public function mcsubscribe(Request $request)
    {
        // die($request->email);
        Mailchimp::subscribe('278c64329a', $request->email, $merge = [], $confirm = true);
        echo('success');
    }

    public function advertise() {
        // if (auth()->user() === null || auth()->user()->role_id !== 1) {
        //     return redirect('/');
        // }
        return view('advertise');
    }
}
