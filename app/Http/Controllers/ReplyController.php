<?php

namespace App\Http\Controllers;

use App\Thread;
use App\Reply;
use App\ThreadFavourites;
use Illuminate\Http\Request;

class ReplyController extends Controller
{
    public function store( Request $request ) {

        $v = \Validator::make($request->all(), [
            'body' => 'required'
        ]);
    
        if ($v->fails())
        {
            return redirect()->back()->withInput()->withErrors($v);
        }

        // dd($request->all());

        $reply = new Reply;
        $reply->body = $request['body'];
        $reply->owner_id = auth()->user()->id;
        $reply->thread_id = $request['thread_id'];
        $reply->quoted_id = $request['quoted'];

        $reply->save();


        $thread = Thread::where('id', $request['thread_id']);
        $thread->update(['updated_at' => $reply->updated_at]);
        
        $redirect = '/forums/' . $request['slug'];
        
        return redirect($redirect);
    }

    public function delete_reply($reply_id) {
        $reply = Reply::where('id', '=', $reply_id)->with('owner')->first();
        $thread = Thread::where('id', '=', $reply->thread_id)->first();
        // dd($thread);
        if($reply->owner->id === auth()->user()->id || auth()->user()->role_id <= 2) {
            $reply->delete();
            return redirect('/forums/' . $thread->slug);
        } else {
            die('nope');
        }
    }

    public function edit_reply($reply_id) {
        $reply = Reply::where('id', '=', $reply_id)->with('owner')->first();
        $thread = Thread::where('id', '=', $reply->thread_id)->first();
        if($reply->owner->id === auth()->user()->id || auth()->user()->role_id <= 2) {
            return view('forums.threads.edit_reply', ['reply' => $reply, 'thread' => $thread]);
        } else {
            die('nope');
        }
    }

    public function update_reply(Request $request) {
        $reply = Reply::where('id', '=', $request->reply_id)->with('owner')->first();
        $thread = Thread::where('id', '=', $reply->thread_id)->first();

        if($reply->owner->id === auth()->user()->id || auth()->user()->role_id <= 2) {
            $reply->body = $request['body'];
            $reply->save();
            return redirect('/forums/' . $thread->slug);
        } else {
            die('nope');
        }
    }

    
    public function vote(Request $request) {
        $userId = auth()->user()->id;
        if($request->requestType === 'add'){
            $favourite = ThreadFavourites::where('user_id', '=', $userId)
            ->where('thread_id', $request['thread_id'])
            ->first();
            if($favourite) {
                $favourite->reply_id = $request['reply_id'];
                $favourite->save();
            } else {
                $favourite = new ThreadFavourites;
                $favourite->user_id = $userId;
                $favourite->reply_id = $request['reply_id'];
                $favourite->thread_id = $request['thread_id'];
                $favourite->save();
            }
            print(json_encode(['user' => $userId, 'reply' => $request['reply_id'], 'thread_id'=> $request['thread_id']]));
        } else {
            $favourite = ThreadFavourites::where('user_id', '=', $userId)
            ->where('thread_id', $request['thread_id'])
            ->where('reply_id', $request['reply_id'])
            ->first();
            $favourite->delete();
            print(json_encode(['user' => $userId, 'reply' => $request['reply_id'], 'thread_id'=> $request['thread_id']]));

        }

    }
}
