<?php

namespace App\Http\Controllers;

use App\Plan;
use Illuminate\Http\Request;

class PlanController extends Controller
{

    public function index()
    {
        $plans = Plan::all();
        return view('plans.index', ['plans'=>$plans]);
    }

    public function show($slug) {
        $user = auth()->user();
        $plan = Plan::where('slug','=',$slug)->first();
        $plans = Plan::all();
        $stripe = \Config::get('app.stripe_key');
        return view('plans.show', ['plan' => $plan, 'plans' => $plans, 'intent' => $user->createSetupIntent(), 'stripe_key' => $stripe]);
    }

    public function thanks() {
        return view('plans.thankyou', ['user' => auth()->user()]);
    }
}
