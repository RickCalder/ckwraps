<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Plan;
use Mailchimp;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/plans';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'plan' => ['required'],
            'found' => ['required'],
            'g-recaptcha-response' => 'required|recaptcha'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        // dd($data);
        $this->redirectTo = '/plans/' . $data['plan'];

        if (isset($data['newsletter'])) {
            Mailchimp::subscribe('278c64329a', $data['email'], $merge = ['FNAME' => $data['name'], 'LNAME' => $data['lastname'], 'USERTYPE' => 'Member'], $confirm = false);
        }

        if( isset($data['lastname'])) {
            $name = $data['name'] . ' ' . $data['lastname'];
        } else {
            $name = $data['name'];
        }
        
        return User::create([
            'name' => $name,
            'email' => $data['email'],
            'found' => $data['found'],
            'password' => Hash::make($data['password'])
        ]);
    }

    public function showRegistrationForm()
    {
        $plans = Plan::all();
        return view('auth.register', compact('plans'));
    }
}
