<?php

namespace App\Http\Controllers;

use App\PageData;
use App\Giveaway;
use App\Video;
use App\LiveEvent;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        $url = explode("/",url()->current());
        if(!isset($url[3])) {
            $pageData = PageData::where('page_slug','=', 'home')->first();
        } else {
            $slug = $url[3];
            $pageData = PageData::where('page_slug','=', $slug)->first();
            if($pageData === null) {
                $pageData = PageData::where('page_slug','=', 'default')->first();
            }
        }
        $giveaway = Giveaway::where('published', '=', '1')->first();
        // dd($pageData);

        $videos = Video::all();
        $liveEvent = LiveEvent::where('live', true)->get();
        // dd($liveEvent->count());
        $totalSeconds = 0;
        foreach( $videos as $video ) {
            $totalSeconds += ($video->hours * 60 *60) + ($video->minutes * 60) + $video->seconds;
        }
        $time = gmdate("H:i:s", $totalSeconds);
        $video_data = [
            'hours' => (int)floor($totalSeconds / 3600),
            'minutes' => (INT)floor(($totalSeconds %3600)/60),
            'seconds' => $totalSeconds % 60,
            'count' => count($videos)
        ];
        $downloads = \App\Downloads::all();
        $allowed = \Request::get('userAllowed');

        \View::share(['allowed' => $allowed, 'pageData' => $pageData, 'giveaway' => $giveaway, 'video_data' => $video_data, 'downloads' => $downloads, 'liveEvent' =>$liveEvent]);

        
     }
}
