<?php

namespace App\Http\Controllers;

use App\Review;
use App\Plan;
use App\UserAvatar;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{


  public function account()
  {
    $user = auth()->user();
    return view('auth.account', ['user' => $user]);
  }

  public function avatar(Request $request) {
    // $request->avatar->store('avatars');
    $user = auth()->user();

    $request->validate( [
      'avatar' => 'required|mimes:jpeg,jpg,png|max:1024', //1MB 
    ]);
    $uploadedFile = $request->file('avatar');
    $filename = time().$uploadedFile->getClientOriginalName();
    $path = $request->file('avatar')->storeAs('public/avatars',$filename);

    $userfile = UserAvatar::where('user_id', '=', $user->id)->first();
    if($userfile) {
      unlink(storage_path('app/public/avatars/'.$userfile->image));
      $userfile->delete();
    }

    $userAvatar = new \App\UserAvatar;
    $userAvatar->user_id = $user->id;
    $userAvatar->image = $filename;
    $userAvatar->save();

    
    $user = auth()->user();
    return back()->withInput();
    return view('auth.account', ['user' => $user]);
  }

  public function subscription() {
    define("SUB", 'CkWraps Subscription');
    $user = auth()->user();
    // dd($user->subscription('CkWraps Subscription'));

    // dd($user);
    if( $user->subscriptions(SUB)->first() !== null ) {

      $subscription = $user->subscriptions(SUB)->first();
      $plan = Plan::where('stripe_plan', '=', $subscription->stripe_plan)->first();
      $plans = Plan::all();
      $intent = $user->createSetupIntent();
      $stripe = \Config::get('app.stripe_key');

      return view('auth.subscription', ['user' => $user, 'subscription' => $subscription, 'plan' => $plan, 'intent' => $intent, 'stripe_key' => $stripe]);
    }
    return view('auth.subscription', ['user' => $user, 'subscription' => null, 'plan' => 'free']);
  }
  
  public function address(Request $request) {
    $user = auth()->user();
    $user->address1 = $request->address1;
    $user->address2 = $request->address2;
    $user->city = $request->city;
    $user->state = $request->state;
    $user->country = $request->country;
    $user->zip = $request->zip;
    $user->save();
    return back()->withInput();
  }


  public function review() {
    $user = auth()->user();
    $review = Review::where('user_id', '=', $user->id)->first();

    return view('auth.review', ['user' => $user, 'review'=> $review]);

  }

  public function create_review(Request $request) {
    $v = \Validator::make($request->all(), [
      'name' => 'required',
      'body' => 'required',
    ]);

    if ($v->fails()) {
      return redirect()->back()->withInput()->withErrors($v);
    }

    $user = auth()->user();
    $review = new Review();
    $review->user_id = $user->id;
    $review->name = $request['name'];
    $review->body = $request['body'];
    $review->published = 0;

    $review->save();
    return redirect()->back()->with('status', 'Thank you for your review! We may contact you regarding your review, or publish your review to our testimonials page');
  }
  public function update_review(Request $request) {
    $user = auth()->user();
    $review = Review::where('user_id', '=', $user->id)->first();
    $review->name = $request['name'];
    $review->body = $request['body'];
    $review->save();
    return redirect()->back()->with('status', 'Your review was successfully updated.');
  }

  public function update(Request $request)
  {
    $user = auth()->user();
    $v = \Validator::make($request->all(), [
      'name' => 'required',
      'email' => [
        'required',
         Rule::unique('users')->ignore($user->id),
      ],
    ]);

    if ($v->fails()) {
      return redirect()->back()->withInput()->withErrors($v);
    }
    // dd($request->all());
    if(!$request['notifications']) {
      $notifications = 0;
    } else {
      $notifications = 1;
    }
    $user->name = $request['name'];
    $user->email = $request['email'];
    $user->notifications = $notifications;
    $user->save();
    // dd($user);

    return redirect()->back()->with('status', 'Your details were successfully updated.');
  }
}
