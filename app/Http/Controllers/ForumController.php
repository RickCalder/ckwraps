<?php

namespace App\Http\Controllers;

use App\ForumLikes;
use App\Reply;
use App\ReplyLikes;
use App\Thread;
use App\ThreadTag;
use App\ThreadFavourites;
use \DB;
use Illuminate\Http\Request;

class ForumController extends Controller
{


    public function overview()
    {
        $tags = ThreadTag::orderBy('tag_name')->get();

        $allowed = \Request::get('userAllowed');
        
        $threads = Thread::with('replies', 'replies.owner', 'tag', 'user', 'likes', 'replies.likes')
            ->whereHas('user', function($q) {
                $q->where('role_id', '!=', 3);
            })
            ->where('announcement', '=', 0)
            ->orderBy('updated_at', 'DESC')
            ->paginate(25);
        $announcements = Thread::with('replies', 'replies.owner', 'tag', 'user', 'likes', 'replies.likes')
            ->where('announcement', '>=', 1)
            ->orderBy('updated_at', 'DESC')
            ->paginate(25);

        return view('forums.overview', ['searchTerm' =>'','tags' => $tags, 'threads' => $threads, 'allowed' => $allowed, 'current_tag' => null,'announcements'=> $announcements]);
    }

    public function search(Request $request) {
        $q = $request->input('query');
        
        $tags = ThreadTag::orderBy('tag_name')->get();

        $allowed = \Request::get('userAllowed');
        if($q !== null) {
            $threads = Thread::where( function($query) use($q) {
              $query->where ( 'title', 'LIKE', '%' . $q . '%' )
              ->orWhere ( 'body', 'LIKE', '%' . $q . '%' );
            })
            ->orderBy('created_at', 'desc')
              ->paginate (25)->setPath ( '' );
            $pagination = $threads->appends ( array (
              'query' => $request->input('query'),
              'category' => $request->input('category')
            ) );

            // dd($threads);
            return view('forums.search', ['searchTerm' =>$q,'tags' => $tags, 'threads' => $threads, 'allowed' => $allowed, 'current_tag' => null]);
        }
    }

    public function my_threads() {
        if (! \Request::get('userAllowed')) {
            return redirect('forums');
        }

        $tags = ThreadTag::orderBy('tag_name')->get();


        $allowed = \Request::get('userAllowed');
        
        if (! \Request::get('userAllowed')) {
            return redirect('forums');
        }

        $threads = Thread::with('replies', 'replies.owner', 'tag', 'user')
        ->where('user_id', '=', Auth()->user()->id)
            ->orderBy('updated_at', 'DESC')
            ->paginate(25);

        // dd($threads);

        return view('forums.users.show', ['searchTerm' =>'', 'tags' => $tags, 'threads' => $threads, 'allowed' => $allowed]);

    }

    public function my_replies() {
        if (! \Request::get('userAllowed')) {
            return redirect('forums');
        }

        $tags = ThreadTag::orderBy('tag_name')->get();


        $allowed = \Request::get('userAllowed');
        
        if (! \Request::get('userAllowed')) {
            return redirect('forums');
        }

        // $threads = Thread::with('replies', 'replies.owner', 'tag', 'user')
        //     ->where('replies.owner_id', '=', 1)
        //     ->orderBy('updated_at', 'DESC')
        //     ->paginate(25);


        $threads = Reply::with('thread')->where('owner_id', '=', Auth()->user()->id)->orderBy('id','DESC')->paginate(25);


            // dd($threads);

        return view('forums.users.replies', ['searchTerm' =>'','tags' => $tags, 'threads' => $threads, 'allowed' => $allowed]);

    }

    public function show($slug)
    {
        if (! \Request::get('userAllowed')) {
            return redirect('forums');
        }
        // dd(\Request::get('userAllowed'));
        $thread = Thread::where('slug', $slug)->with('user', 'replies', 'replies.favourites', 'replies.quoted')->first();

        $my_fave = ThreadFavourites::where('user_id', '=', auth()->user()->id)
        ->where('thread_id', '=', $thread->id)
        ->first();
        $likes = ForumLikes::where('item_id', '=', $thread->id);
        $like_check = ForumLikes::where('user_id', '=', auth()->user()->id)->where('item_id', '=', $thread->id)->first();
        if($like_check) {
            $liked='like';
        }else {
            $liked='nolike';
        }
        $replyLikes = ReplyLikes::where('user_id', '=', auth()->user()->id)->get();
        $myLikes = [];
        foreach($replyLikes as $like) {
            array_push($myLikes, $like->item_id);
        }
        return view('forums.threads.show', ['thread' => $thread, 'my_fave' => $my_fave, 'likes' => $likes, 'liked' => $liked,'my_likes' => $myLikes]);
    }

    public function create(Request $request)
    {
        if (! \Request::get('userAllowed')) {
            return redirect('forums');
        }

        if(auth()->user()->role_id === 3) {
            abort(403, 'Access denied');
        }
        $tags = ThreadTag::orderBy('tag_name')->get();
        $currentTag = ThreadTag::where('tag_slug', $request->slug)->get();
        return view('forums.threads.create', ['tags' => $tags, 'currentTag' => $currentTag]);
    }

    public function store(Request $request)
    {
        // dd($request->all());
        
        if (! \Request::get('userAllowed')) {
            return redirect('forums');
        }

        if(auth()->user()->role_id === 3) {
            abort(403, 'Access denied');
        }

        $v = \Validator::make($request->all(), [
            'title' => 'required|max:100',
            'body' => 'required',
            'tag' => 'required'
        ]);

        if ($v->fails()) {
            return redirect()->back()->withInput()->withErrors($v);
        }

        $thread = new Thread;
        $thread->title = $request['title'];
        // $thread->slug = str_slug($request['title'], '-') . '-' . rand(1,1000);
        $thread->announcement = $request['announcement'];
        $thread->vote_expiry = $request['vote_expiry'];
        $thread->body = $request['body'];
        $thread->user_id = auth()->user()->id;
        $thread->tag_id = $request['tag'];

        $thread->save();

        return redirect('forums');
    }

    public function show_user($user_id)
    {

        $tags = ThreadTag::orderBy('tag_name')->get();


        $allowed = \Request::get('userAllowed');
        
        if (! \Request::get('userAllowed')) {
            return redirect('forums');
        }

        $threads = Thread::with('replies', 'replies.owner', 'tag', 'user')
        ->where('user_id', '=', $user_id)
            ->orderBy('updated_at', 'DESC')
            ->paginate(25);

        // dd($threads);

        return view('forums.users.show', ['searchTerm' =>'','tags' => $tags, 'threads' => $threads, 'allowed' => $allowed]);
    }

    public function delete_thread($thread_id)
    {
        $thread = Thread::where('id', '=', $thread_id)->with('user')->first();
        // dd($thread);
        if ($thread->user->id === auth()->user()->id || auth()->user()->role_id <= 2) {
            $thread->delete();
            return redirect('/forums');
        } else {
            die('nope');
        }
    }

    public function edit($slug)
    {
        $thread = Thread::where('slug', '=', $slug)->with('user')->first();
        if ($thread->user->id === auth()->user()->id || auth()->user()->role_id <= 2) {
            $tags = ThreadTag::orderBy('tag_name')->get();
            return view('forums.threads.edit', ['tags' => $tags, 'thread' => $thread]);
        } else {
            die('nope');
        }
    }

    public function update_thread(Request $request)
    {

        // dd($request->all());
        $thread = Thread::where('id', '=', $request->thread_id)->with('user')->first();
        if ($thread->user->id === auth()->user()->id || auth()->user()->role_id <= 2) {
            $thread->title = $request['title'];
            $thread->body = $request['body'];
            $thread->tag_id = $request['tag'];
            $thread->vote_expiry = $request['vote_expiry'];
            $thread->announcement = $request['announcement'];

            $thread->save();
            return redirect('/forums/' . $thread->slug);
        } else {
            die('nope');
        }
    }

    public function like_thread(Request $request) {
        $user = auth()->user()->id;
        $already = ForumLikes::where('user_id', '=', $user)
        ->where('item_id', '=', $request->thread)
        ->first();

        if(!$already) {
            $like = new ForumLikes;
            $like->type = $request->type;
            $like->item_id = $request->thread;
            $like->user_id = $user;
            $like->save();
        } else {
            $already->delete();
        }
    }

    public function like_reply(Request $request) {
        $user = auth()->user()->id;
        $already = ReplyLikes::where('user_id', '=', $user)
        ->where('item_id', '=', $request->reply)
        ->first();
        if(!$already) {
            $like = new ReplyLikes;
            $like->type = 0;
            $like->item_id = $request->reply;
            $like->user_id = $user;
            $like->save();
        } else {
            $already->delete();
        }
    }





}
