<?php

namespace App\Http\Controllers;

use App\Review;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
  public function index()
  {
    $reviews = Review::where('published', '=', 1)->orderBy('created_at', 'DESC')->paginate(12);
    return view('testimonials', ['reviews'=>$reviews]);
  }

}
