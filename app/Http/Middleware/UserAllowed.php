<?php

namespace App\Http\Middleware;

use Closure;

class UserAllowed
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        $user = auth()->user();
        if($user){
            $paypal_end_date = $user->paypal_end_date;
            if($paypal_end_date !== null) {
                if($paypal_end_date < now()) {
                    $user->role_id = 3;
                    $user->save();
                }
            }
            $sub = $user->subscribed('CkWraps Subscription');
            $user_role = $user->role_id;
            if( $user_role === 1 || $user_role === 4 || $sub !== false) {
                $request->attributes->add(['userAllowed' => true]);
            } else {
                $request->attributes->add(['userAllowed' => false]);
            }
        } else {
            $sub = null;
            $user_role = null;
            $request->attributes->add(['userAllowed' => false]);
        }


        // dd($user);?
        return $next($request);
    }
}