<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ThreadFavourites extends Model
{
    public function owner() {
        return $this->belongsTo('App\User');
    }

    public function reply() {
        return $this->belongsTo('App\Reply');
    }
}
