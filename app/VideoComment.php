<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoComment extends Model
{
    public function user() {
        return $this->belongsTo('App\User');
    }

    public function quoted() {
        return $this->hasOne('App\VideoComment','id', 'quoted_id');
    }
}
