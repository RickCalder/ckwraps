@extends('layouts.app')


@section('content')
<div class="container" style="margin-top: 100px">
  <div class="row">
    <div class="col-lg-8 offset-lg-2">
      @if(! isset($giveaway))
      <h1 class="h2">Sorry, there are no current giveaways, please check back soon!</h1>
      @else
      <h1>{{$giveaway->title}}</h1>
      <div class="mt-4">
                        
      <div class="alert alert-info offset-md-2 col-md-8">
        <p class="mb-0">Please note, any coupons or promotions are only available via Credit Card payment. They cannot be applied to a PayPal payment.</p>
      </div>
        {!! $giveaway->body !!}
      </div>
      @endif
    </div>
  </div>
  @endsection