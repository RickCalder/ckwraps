@extends('layouts.app')


@section('content')
<div class="container" style="margin-top: 100px">
    <div class="row">
      <div class="col-md-12">
        <h1>Privacy Policy</h1>

        <p><strong>Please read this privacy policy carefully as it will help you make informed decisions about sharing your personal information with us.</strong></p>

        <h2>1. WHAT INFORMATION DO WE COLLECT?</h2>

        <p><strong>Personal Information you disclose to us</strong></p>

        <p><em>In Short: We collect personal information that you provide to us such as name, address, contact information, passwords and security data, and payment information.</em></p>

        <p>We collect personal information that you voluntarily provide to us when registering at the Apps, expressing an interest in obtaining information about us or our products and services, when participating in activities on the Apps or otherwise contacting us.

        <p>The personal information that we collect depends on the context of your interactions with us and the Apps, the choices you make and the products and features you use. The personal information we collect can include the following:</p>
        <p>Name and Con tact Data.We collect your first and last name, email address, postal address, phone number, and other similar contact data.</p>

        <p>Credentials. We collect passwords, password hints, and similar security information used for authentication and account access.</p>

        <p>Payment Data. We collect data necessary to process your payment if you make purchases, such as your payment instrument number (such as a credit card number), and the security code associated with your payment instrument. All payment data is stored by our payment processor and you should review its privacy policies and contact the payment processor directly to respond to your questions.  </p>

        <p>All personal information that you provide to us must be true, complete and accurate, and you must notify us of any changes to such personal information.</p>   

        <p>Information collected through our Apps</p>

        <p>In Short: We may collect information regarding your push notifications, when you use our apps.</p>

        <p>If you use our Apps, we may also collect the following information:</p>       

        <ul><li>Push Notifications. We may request to send you push notifications regarding your account or the website. If you wish to opt-out from receiving these types of communications, you may turn them off in your device’s settings.</li></ul>       
        <p>Information collected from other sources</p>

        <p>In Short: We may collect limited data from public databases, marketing partners, and other outside sources.</p>
        <p>We may obtain information about you from other sources, such as public databases, joint marketing partners, as well as from other third parties. Examples of the information we receive from other sources include: social media profile information; marketing leads and search results and links, including paid listings (such as sponsored links).</p>

        <h2>2. HOW DO WE USE YOUR INFORMATION?</h2> 

        <p>In Short: We process your information for purposes based on legitimate business interests, the fulfillment of our contract with you, compliance with our legal obligations, and/or your consent.</p>

        <p>We use personal information collected via our Apps for a variety of business purposes described below. We process your personal information for these purposes in reliance on our legitimate business interests (“Business Purposes”), in order to enter into or perform a contract with you (“Contractual”), with your consent (“Consent”), and/or for compliance with our legal obligations (“Legal Reasons”). We indicate the specific processing grounds we rely on next to each purpose listed below.</p>

        <p>We use the information we collect or receive:</p>
        <ul>
          <li>To facilitate account creation and logon processwith your Consent. If you choose to link your account with us to a third party account *(such as your Google or Facebook account), we use the information you allowed us to collect from those third parties to facilitate account creation and logon process.</li> 

          <li>To send you marketing and promotional communicationsfor Business Purposes. We and/or our third party marketing partners may use the personal information you send to us for our marketing purposes, if this is in accordance with your marketing preferences. You can opt-out of our marketing emails at any time (see the “WHAT ARE YOUR PRIVACY RIGHTS” below).</li>

          <li>Fulfill and manage your ordersfor Contractual reasons. We may use your information to fulfill and manage your orders, payments, returns, and exchanges made through the Apps.</li>  

          <li>Delivering targeted advertising to youfor our Business Purposes. We may use your information to develop and display content and advertising (and work with third parties who do so) tailored to your interests and/or location and to measure its effectiveness.</li>     

          <li>Request Feedbackfor our Business Purposes. We may use your information to request feedback and to contact you about your use of our Apps.  </li>  

          <li>To enforce our terms, conditions and policiesfor Business Purposes, Legal Reasons and/or possibly Contractual.</li>

          <li>To respond to legal requests and prevent harmfor Legal Reasons. If we receive a subpoena or other legal request, we may need to inspect the data we hold to determine how to respond. </li> 

          <li>For other business purposes. We may use your information for other Business Purposes, such as data analysis, identifying usage trends, determining the effectiveness of our promotional campaigns and to evaluate and improve our Apps, products, services, marketing and your experience.</li>
        </ul>

        <h2>3. WILL YOUR INFORMATION BE SHARED WITH ANYONE?</h2>

        <p>In Short: We only share information with your consent, to comply with laws, to protect your rights, or to fulfill business obligations.</p>

        <p>We only share and disclose your information in the following situations:</p>
        <ul>

          <li>Compliance with Laws. We may disclose your information where we are legally required to do so in order to comply with applicable law, governmental requests, a judicial proceeding, court order, or legal process, such as in response to a court order or a subpoena (including in response to public authorities to meet national security or law enforcement requirements).</li>

          <li>Vital Interests and Legal Rights.We may disclose your information where we believe it is necessary to investigate, prevent, or take action regarding potential violations of our policies, suspected fraud, situations involving potential threats to the safety of any person and illegal activities, or as evidence in litigation in which we are involved.  </li>

          <li>Vendors, Consultants, and Other Third-Party Service Providers.We may share your data with third party vendors, service providers, contractors or agents who perform services for us or on our behalf and require access to such information to do that work. Examples include: payment processing, data analysis, email delivery, hosting services, customer service and marketing efforts. We may allow selected third parties to use tracking technology on the Apps, which will enable them to collect data about how you interact with the Apps over time. This information may be used to, among other things, analyze and track data, determine the popularity of certain content and better understand online activity. Unless described in this Policy, we do not share, sell, rent or trade any of your information with third parties for their promotional purposes.   </li>

          <li>Business Transfers.We may share or transfer your information in connection with, or during negotiations of, any merger, sale of company assets, financing, or acquisition of all or a portion of our business to another company.</li> 

          <li>Third-Party Advertisers. We may use third-party advertising companies to serve ads when you visit the Apps. These companies may use information about your visits to our Website(s) and other websites that are contained in web cookies and other tracking technologies in order to provide advertisements about goods and services of interest to you.     </li>

          <li>Affiliates. We may share your information with our affiliates, in which case we will require those affiliates to honor this privacy policy. Affiliates include our parent company and any subsidiaries, joint venture partners or other companies that we control or that are under common control with us.     </li>

          <li>Business Partners.We may share your information with our business partners to offer you certain products, services or promotions.   </li>

          <li>With your Consent.We may disclose your personal information for any other purpose with your consent.</li>  
        </ul> 

        <h2>4. HOW LONG DO WE KEEP YOUR INFORMATION?</h2> 

        <p>In Short: We keep your information for as long as necessary to fulfill the purposes outlined in this privacy policy unless otherwise required by law.</p>

        <p>We will only keep your personal information for as long as it is necessary for the purposes set out in this privacy policy, unless a longer retention period is required or permitted by law (such as tax, accounting or other legal requirements). No purpose in this policy will require us keeping your personal information for longer than 2 years past the termination of the user’s account.</p>

        <p>When we have no ongoing legitimate business need to process your personal information, we will either delete or anonymize it, or, if this is not possible (for example, because your personal information has been stored in backup archives), then we will securely store your personal information and isolate it from any further processing until deletion is possible.</p>

        <h2>5. HOW DO WE KEEP YOUR INFORMATION SAFE?</h2>  

        <p>In Short: We aim to protect your personal information through a system of organizational and technical security measures.</p>

        <p>We have implemented appropriate technical and organizational security measures designed to protect the security of any personal information we process. However, please also remember that we cannot guarantee that the internet itself is 100% secure. Although we will do our best to protect your personal information, transmission of personal information to and from our Apps is at your own risk. You should only access the services within a secure environment.</p>

        </div>
      </div>
    </div>

@endsection
