<div class="container mt-5 scroll-target p-4">
  <p class="text-center h2" style="font-family: sans-serif">
    <i class="fa fa-video-camera"></i> &nbsp;{{$video_data['count']}} high quality educational videos and counting
  </p>
  <div class="d-flex justify-content-center mt-3">
    <div class="timer-container">
      <p>
      {{$video_data['hours']}}
    </p>
    <p class="timer-text">
      hours
    </p>
    </div>
    <div class="timer-container">
      <p>
      {{$video_data['minutes']}}
    </p>
    <p class="timer-text">
      min
    </p>
    </div>
    <div class="timer-container">
      <p>
      {{$video_data['seconds']}}
    </p>
    <p class="timer-text">
      sec
    </p>
    </div>
  </div>
  <p class="text-center mt-4">
    @if($allowed)
    <a href="/videos" class="btn btn-lg btn-primary">Watch Now!</a>
    @else
    <a href="/register" class="btn btn-lg btn-primary">Join Now!</a><br>
    <a href="/login"><small>or login to your account</small></a>
    @endif
  </p>
</div>