<div>
  <a href="/videos/{{$rec->id}}">
    <div class="row mb-3" style="font-size: 0.85rem">
      <div class="mb-3 col-6">
        @if($rec->thumbnail !== null)
        <img src="{{Storage::url($rec->thumbnail)}}" class="card-img-top-v">
        @else
        @if($rec->source === 'vimeo')
        <VideoThumbnail url="{{$rec->private_link}}" />
        @else
        <img src="https://img.youtube.com/vi/{{$rec->video_id}}/0.jpg" class="card-img-top-v">
        @endif
        @endif
      </div>
      <div class="col-6">
        <h2 class="h5 text-dark">{{ $rec->title }}</h2>
        <p class="text-dark">{{ \Illuminate\Support\Str::limit(strip_tags($rec->description), 65, $end='...') }}</p>
      </div>
    </div>
  </a>
</div>