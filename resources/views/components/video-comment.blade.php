<div class="mt-4">
  <div  class="d-flex align-items-center justify-content-md-between">
    <div class="d-flex">
  @if($comment->user->avatar)
  <div class="d-flex align-items-center">
    <img src="{{ Storage::url('/avatars/'.$comment->user->avatar->image) }}" class="avatar avatar-xs" />
  </div>
  @else
  <div class="avatar avatar-xs avatar-initials d-flex align-items-center justify-content-center">
    <span style="line-height:0">
      {{substr($comment->user->name,0,1)}}
    </span>
  </div>
  @endif
  <p class="mb-0 ml-2">{{$comment->user->name}} <span style="font-size:0.875rem">{{ $comment->created_at->diffForHumans() }}</span></p>
</div>
  <div class="d-flex align-items-center">
  @if( auth()->user()->id === $comment->user->id || auth()->user()->role_id <= 2)
    {{ Form::open(['route' => ['videocomment.delete', $comment->id], 'method' => 'post', 'id' => 'delete-form']) }}
      @csrf
      <button style="border:none; font-size: 2rem; margin-right: 1rem" id="delete-comment" class="ml-3"><i class="fa fa-trash text-danger" title="Delete this comment"></i><span class="sr-only">Delete this comment</span></button>
    {{ Form::close() }}
    {{ Form::open(['route' => ['videocomment.edit', $comment->id], 'method' => 'get']) }}
      @csrf
      <button type="submit" class="ml-3"><i class="fa fa-edit" title="Edit this reply"></i><span class="sr-only">Edit this reply</span></button>
    {{ Form::close() }}
    @endif
  </div>
  </div> 
  <p class="mt-3">{{$comment->comment}}</p>
  @if($comment->quoted_id !== 0)
    @if($comment->quoted->deleted_at === null)
    <div class="video-quote mb-2">
      <p class="mb-1">
        Replying to: {{$comment->quoted->user->name}} <span style="font-size:0.875rem">{{ $comment->quoted->created_at->diffForHumans() }}</span>
      </p>
      {{$comment->quoted->comment}}
    </div>
    @else
    <div class="video-quote mb-2">
      <p class="mb-1">
        Replying to: {{$comment->quoted->user->name}} <span style="font-size:0.875rem">{{ $comment->quoted->created_at->diffForHumans() }}</span>
      </p>
      This comment was deleted.
    </div>
    @endif
  @endif
  <div class="d-flex justify-content-end" style="line-height: 1">
    @auth

@if( auth()->user()->id === $comment->user_id || auth()->user()->role_id <= 2)


@endif
@endauth
    <button class="reply btn btn-success btn-sm" data-commentid="{{$comment->id}}">Reply</button>
  </div>
</div>
<hr>


