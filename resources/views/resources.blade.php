@extends('layouts.app')


@section('content')
<div class="container" style="margin-top: 100px">
  <div class="row">
    <div class="col-md-12">
      <h1 class="mb-5">Vinyl Wrap Resources</h1>
    </div>
  </div>
  @foreach ($sponsors as $sponsor)
  <div class="row">
    <div class="col-md-2">
      <img 
        class="" 
        src="{{Storage::url($sponsor->image)}}" 
        alt="Image of {{ $sponsor->name }} logo"
        style="max-width: 100%;">
    </div>
    <div class="col-md-10">
      <h2 class="card-title">{{ $sponsor->name }}</h2>
      <p class="card-text">{{ $sponsor->description }}</p>
      <h3 class="h5">{{ $sponsor->name }} Links</h3>
      <ul>
        <li><a href="{{ $sponsor->link }}" target="_blank" title="{{ $sponsor->name }} Homepage">{{ $sponsor->name }} Homepage</a></li>
        @foreach($sponsor->links as $link)
          <li><a href="{{ $link->link_url }}" target="_blank" title="{{ $link->link_text }} Homepage">{{ $link->link_text }}</a></li>
        @endforeach
      </ul>
    </div>
  </div>
  <hr>
  @endforeach
</div>

@endsection