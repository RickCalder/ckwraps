<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @include('partials.extra-head-tags')

    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}" defer></script>
    @yield('extra-head')

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700|Raleway:600&display=swap"
        rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
        integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-51219319-3"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-51219319-3');
    </script>

    <!-- Global site tag (gtag.js) - Google Ads: 611911809 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-611911809"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'AW-611911809');
    </script>


</head>

<body>
    <div id="app" class="d-flex flex-column">
        @include('partials.main-nav')
        @yield('top-content')

        <main>
            @yield('content')
            <!-- Modal -->
            <div class="modal fade" id="addressModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Billing Address</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body">
                    <p>We are doing some housekeeping, and require registered member's billing addresses. Please follow this <a href ="/my-account/subscription" class="btn btn-sm btn-primary">link</a> to update yours!</p>
                    <p>Thank you!</p>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
                </div>
            </div>
        </main>
        @include('partials.footer')

    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous">
    </script>
    @stack('scripts')
    <script>
        $(window).on('load', function() {
            let user = {!! json_encode(auth()->user() ) !!}
            if(user != null) {
                let address1 = user.address1;
                let role = user.role_id;
                let read = getCookie('read')
                if((typeof(address1) === 'undefined' || address1 == null || address1 === '') && role === 4 && read !== 'true' ) {
                    $('#addressModal').modal('show');
                    document.cookie = "read=true; max-age=" + 24*60*60;
                }
            }
            
        });

        function getCookie(name) {
            // Split cookie string and get all individual name=value pairs in an array
            let cookieArr = document.cookie.split(";");
            
            // Loop through the array elements
            for(let i = 0; i < cookieArr.length; i++) {
                let cookiePair = cookieArr[i].split("=");
                
                /* Removing whitespace at the beginning of the cookie name
                and compare it with the given string */
                if(name == cookiePair[0].trim()) {
                    // Decode the cookie value and return
                    return decodeURIComponent(cookiePair[1]);
                }
            }
            
            // Return null if not found
            return null;
        }
    </script>

    <script>
        window.addEventListener('load',function(){
        if(window.location.pathname == "/thank-you"){
            gtag('event', 'conversion', {'send_to': 'AW-611911809/Sbw1CMqHud4CEIGR5KMC'});
        }
        });
    </script>
</body>

</html>