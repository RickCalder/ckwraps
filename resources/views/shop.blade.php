@extends('layouts.app')


@section('content')
<div class="container" style="margin-top: 100px">
  <div class="row">
    <div class="col-md-12">
      <h1 class="mb-5">Vinyl Wrap Tools &amp; Accessories</h1>
    </div>
    <div class="card-deck product-cards">
      @foreach ($products as $product)
      <div class="card">
        <img class="card-img-top" src="{{ $product->image }}" alt="Image of {{ $product->name }}">
        <div class="card-body">
          <h2 class="card-title h5">{{ $product->name }}</h2>
          <p class="card-text">{{ $product->description }}</p>
        </div>
        <div class="card-footer text-center">
          <a rel="noopener noreferrer" href="{{ $product->url }}" target="_blank" class="btn btn-primary mt-3">Buy Now!
            <i class="fa fa-external-link ml-2"></i> </a>
        </div>
      </div>
      @endforeach
    </div>
  </div>
</div>

@endsection