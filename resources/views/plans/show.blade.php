@extends('layouts.app')

@section('content')
<div class="container" style="margin-top:100px">
  <div class="row">
    <div class="col-xl-12">
      <h1 class="text-center mb-2">Thank you for choosing our {{$plan->name}} subscription plan.</h1>
      <p class="text-center mb-5">
        You will be charged ${{ number_format($plan->cost, 2) }} per {{ $plan->recurring }} for our {{ $plan->name }} Plan
      </p>
    </div>
  </div>
  <div id="choose-container">
    <h2 class="mx-auto text-center mb-3">Choose Payment Type</h2>
    <div class="d-flex justify-content-center" style="gap: 3rem">
      <div class="d-flex flex-column align-items-center pay-type" data-paytype="cc">
        <i class="fa fa-credit-card" style="font-size: 3rem"></i>
        <p>Credit/Debit Card</p>
      </div>
      <div class="d-flex flex-column align-items-center pay-type" data-paytype="pp">
        <i class="fa fa-paypal" style="font-size: 3rem"></i>
        <p>PayPal</p>
      </div>
    </div>
  </div>
  <div class="row justify-content-center pay-container cc-container">
    <div class="col-lg-8 offset-lg-2">
      @include('plans.payment_partial')
    </div>
  </div>
  <div class="row justify-content-center pay-container pp-container">
    <div class="col-lg-8 offset-lg-2">
      @include('plans.paypal_payment_partial')
    </div>
  </div>
</div>
@endsection

@push('scripts')
<script>
  $('.pay-type').on('click', function(e) {
    e.preventDefault()
    let type = $(this).data('paytype')
    $('.pay-container').hide()
    $('#choose-container').hide()
    $('.' + type + '-container').show()
  })
</script>
@endpush