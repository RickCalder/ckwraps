<div class="row justify-content-center">
  <div class="col-md-12">
    @if($errors->any())
      <h4>{{$errors->first()}}</h4>
    @endif
    <form action="{{ route('subscription.create') }}" method="post" id="payment-form">
    <div>
      <h2>Billing Address</h2>
      <div class="form-group">
        <label for="address-1">Address line 1</label><span style="color: red !important; display: inline; float: none;">*</span>
        <input type="text" class="form-control" id="address-1" name="address1" required>
        <label for="address-2">Address line 2</label>
        <input type="text" class="form-control" id="address-2" name="address2">
        <label for="City">City</label><span style="color: red !important; display: inline; float: none;">*</span>
        <input type="text" class="form-control" id="city" name="city" required>
        <label for="state">State/Province</label>
        <input type="text" class="form-control" id="state" name="state">
        @include('partials.countries')
        <label for="zip">Postal/Zip Code</label>
        <input type="text" class="form-control" id="zip" name="zip">
      </div>
      <div>
        <span style="color: red !important; display: inline; float: none;">*</span> denotes required field
      </div>
    </div>
    <h2 class="mt-3">Credit Card Information</h2>
    <div class="card">
        @csrf
        <div class="form-group">
          <div class="card-header">
            <label for="card-element">
              Enter your credit card information<br>
              <small class="text-muted">MM/YY is your card month and year expiry. CVC is the 3 digit number on the back of your card</small>
            </label>
          </div>
          <div class="card-body">
            <div id="card-element">
              <!-- A Stripe Element will be inserted here. -->
            </div>
            <!-- Used to display form errors. -->
            <div id="card-errors" class="alert alert-danger mt-3" role="alert" style="display:none"></div>
            <input type="hidden" name="plan" value="{{ $plan->id }}" />
            <input type="hidden" name="payment_method" id="payment_method" />
          </div>
        </div>
        <div class="card-footer">
          <div class="form-group row">
            <label for="coupon-code" class="col-lg-2">Coupon Code</label>
            <div class="col-lg-2">
              <input type="text" class="form-control" name="coupon" id="coupon-code">
            </div>
          </div>
          <button id="cardButton" class="btn btn-dark" type="submit" data-secret={{ $intent->client_secret }}>Pay
            Now</button>
        </div>
    </div>
  </div>
</form>
  <div class="col-md-12 text-center text-muted">
    <p>Price is in US Dollars</p>
  </div>
</div>

@push('scripts')
<script src="https://js.stripe.com/v3/"></script>
<script>
  // Create a Stripe client.
var stripe = Stripe("{{$stripe_key}}");

// Create an instance of Elements.
var elements = stripe.elements();

// Custom styling can be passed to options when creating an Element.
// (Note that this demo uses a wider set of styles than the guide below.)
var style = {
  base: {
    color: '#32325d',
    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
    fontSmoothing: 'antialiased',
    fontSize: '16px',
    '::placeholder': {
      color: '#aab7c4'
    }
  },
  invalid: {
    color: '#fa755a',
    iconColor: '#fa755a'
  }
};

// Create an instance of the card Element.
var card = elements.create('card', {style: style, hidePostalCode:true});

// Add an instance of the card Element into the `card-element` <div>.
card.mount('#card-element');

// Handle real-time validation errors from the card Element.
card.addEventListener('change', function(event) {
  var displayError = document.getElementById('card-errors');
  if (event.error) {
    displayError.textContent = event.error.message;
  } else {
    displayError.textContent = '';
  }
});

var cardButton = document.getElementById('cardButton')
var clientSecret = cardButton.getAttribute('data-secret')

cardButton.addEventListener('click', async (e) => {
  e.preventDefault();
  cardButton.disabled = true
    const { setupIntent, error } = await stripe.handleCardSetup(
        clientSecret, card
    );

    if (error) {
      document.getElementById('card-errors').style.display = 'block'
      document.getElementById('card-errors').textContent = error.message
      cardButton.disabled = false
        console.log(error)
    } else {
      var form = document.getElementById('payment-form');
      var hiddenInput = document.createElement('input');
      hiddenInput.setAttribute('type', 'hidden');
      hiddenInput.setAttribute('name', 'paymentMethod');
      hiddenInput.setAttribute('value', setupIntent.payment_method);
      form.appendChild(hiddenInput);
      form.submit();
    }
});

</script>
@endpush