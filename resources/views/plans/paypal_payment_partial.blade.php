<div class="row justify-content-center">
  <div class="col-md-12">
    @if($errors->any())
      <h4>{{$errors->first()}}</h4>
    @endif
   <a href="{{ url('subscribe/paypal') }}/{{$plan->id}}"><img src="/images/paypal.png" style="margin:0 auto; max-width:50%;display:block" alt="click to pay with PayPal"></a>
  <div class="col-md-12 text-center text-muted">
    <p>Price is in US Dollars</p>
  </div>
</div>
