@extends('layouts.app')

@section('content')
<div class="container" style="margin-top:100px">
    <div class="row justify-content-center">
        <div class="col-lg-12">
            <h1 class="text-center mb-5">CK Wraps Subscription Plans</h1>
            <div class="card-deck plans">
                @foreach($plans as $plan)
                @if( (app('request')->input('upgrade') === '1' && $plan->slug != 'free') || app('request')->input('upgrade') === null)
                <div class="card mb-4">
                    <div class="card-header text-center"><h2 class="h3">CK Wraps {{ $plan->name }} Subscription</h2></div>
                    <div class="card-body">
                        <div class="card-text">
                            <div class="text-center mb-4">
                                @if($plan->slug === "free")
                                <h3>Free</h3>
                                @else
                                <h3>${{ number_format($plan->cost, 2) }}/{{ $plan->recurring }}*</h3>
                                @endif
                            </div>
                            {!! $plan->description !!}
                        </div>
                    </div>
                    <div class="card-footer text-center">
                        <a href="{{ route('plan', $plan->slug) }}" class="btn btn-lg btn-primary plan-button" id="{{$plan->slug}}">Choose {{ $plan->name }}</a>
                    </div>
                </div>

                @endif
                <div class="w-100 d-block d-xl-none"><!-- wrap every 3 on md--></div>
                @endforeach
            </div>
        </div>
        <div class="col-md-12 text-center text-muted">
            <p>All prices are in US Dollars
                <br>*Paid subscriptions are for a minimum of 3 months
                <br>**We host our videos on Vimeo, some countries block access to Vimeo please check if your country does this.
            </p>
        </div>
    </div>
</div>
@endsection
