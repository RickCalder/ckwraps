@extends('layouts.app')

@section('content')
<div class="container" style="margin-top:100px">
  <div class="row">
    <div class="col">
      <h1 class="mb-4">Thank you for Subscribing!</h1>
      <p>As a premium member you have full access to our <a href="{{route('forums')}}">forums</a> and our <a href="{{route('videos')}}">online video courses</a>!</p>
      <p>We also allow premium members that have wrap related businesses to post on our <a href="{{route('locations')}}">Location Board</a>. You can access your personal account information in your <a href="/my-account">account control panel.</a> add your business to the location board on the My Business tab.</p>
    </div>
  </div>
</div>
@endsection