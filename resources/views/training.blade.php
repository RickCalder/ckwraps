@extends('layouts.app')

@section('top-content')

<div class="fullscreen">

  <div class="fullscreen">
    <div class="slide-mask">
        <div class="slide-content">
          <div class="container">
            <h1 class="slide-content-title">Personalized Training</h1>
            <div class="slide-content-body">
                <p>From beginner to advanced, learn vinyl wrapping from certified instructors!</p>
                @include('partials.join_button')
            </div>
          </div>
        </div>
        <div class="slides-control">
           <div class="slides-control-down">
             Learn More
           </div>
         </div>
      </div>
    <img 
      class="cover"
      srcset="/img/labo2-576.jpg 576w, /img/lambo2-1024.jpg 1024w, /img/lambo2-1920.jpg 1920w"
      sizes="(max-width: 576px) 576px,
      (max-width: 1024px) 1024px,
      (max-width: 1920px) 1920px"
     src="/img/blue-wrapped-audi.jpg" alt="Wrapped Lambo">
  </div>
</div>
@endsection

@section('content')
<div class="container mt-5 scroll-target">
  <div class="row">
    <div class="col-lg-5 background-images order-lg-last" style="background-image:url('/img/working-orange-lambo.jpg');background-size:cover"></div>
    <div class="col-lg-7">
      <h2>Personal Training Programs For Color Change</h2>
      <p>Training is very important in any industry. With the proper training, you'll be able to tackle many different obstacles. This training program is strictly for color change applications. Color changes are what we've specialized in for 8 years.</p>
      <p>Our training is catered to your skill level. From a complete beginner who hasn't touched vinyl, to an advanced installer who would like to learn how to handle and install sensitive films such as chrome. We can cover it all. Our color change process is thorough and in depth. We're here to answer all of your questions and help you to succeed.</p>
      <p>For PPF (paint protection film) certification training click <a href="{{route('ppf-training')}}">here</a></p>
    </div>
  </div>
  <div class="row mt-3">
    <div class="col">
      <p class="text-center">
        <a class="btn btn-lg btn-success" href="https://www.vvividhq.com/get-certified" target="_blank" rel="noopener noreferrer">
          Get Certified!
        </a>
      </p>
    </div>
  </div>
  <hr>
  <div class="row">
    <div class="col-md-5 background-images" style="background-image:url('/img/working-1.jpg');background-size:cover"></div>
    <div class="col-lg-7">
      <h2>Level 1: Beginner</h2>
      <p>We were once beginners and we haven't forgotten what it feels like to start out. Even to this day we will handle new products / materials which remind us that there are always times when we may not be totally comfortable with the film and process of installing. We help you build your skill level. By the time your training is finished you'll have a new confidence in vinyl wraps.</p>
      <p>We will build you a foundation of knowledge. At the beginner level you can expect to learn how to disassemble, prepare and wrap. The majority of beginner classes will be to help you learn handling of a squeegee, glassing out the film, the properties of the film, how it handles, it's capabilities / limitations, what can and can not be wrapped, and how to start tackling different objects.</p>
      <p><i>Note: Level 1 & 2 Workshops are combined in one course.</i></p>
    </div>
  </div>
  <div class="row">
    <div class="col">
      <p class="text-center">
        <a class="btn btn-lg btn-success" href="https://www.vvividhq.com/get-certified" target="_blank" rel="noopener noreferrer">
          Get Certified!
        </a>
      </p>
    </div>
  </div>
  <hr>
  <div class="row">
    <div class="col-lg-5 background-images order-lg-last" style="background-image:url('/img/working-2-1.jpg');background-size:cover"></div>
    <div class="col-lg-7">
      <h2>Level 2: Intermediate</h2>
      <p>In our intermediate classes we'll be going over more technical components while using the skills you've learned as a beginner. Anyone can join our intermediate class if you already feel comfortable with a squeegee, and if you've been applying film to surfaces varying in difficulty. In this class we will teach you about cutting / trimming, corners, recesses, inlays and seams.</p>
      <p>The Intermediate class is great for those who want to refine their new found skill. Practice makes perfect and we have some tips and tricks that will make your installs look better, last longer and be more efficient. Learning hands on is super important, we give you a variety of different finishes so that you can get a feel for everything and how each one varies. This class will help you step up your level of professionalism when it comes to installing.</p>
      <p><i>Note: Level 1 & 2 Workshops are combined in one course.</i></p>
    </div>
  </div>
  <hr>
  <div class="row">
    <div class="col-lg-5 background-images" style="background-image:url('/img/working-3.jpg');background-size:cover"></div>
    <div class="col-lg-7">
      <h2>Level 3: Advanced</h2>
      <p>Advanced training is meant to incorporate your working knowledge of film and installation as a foundation to build on. Here you will learn a set of specific skills which will help you understand how sensitive films work and need to be handled, which is much different from most regular films. We recommend that you do not jump right into advanced training without having thorough vinyl installation experience. This means you should have a few dozen full vehicle wraps under your belt before attempting this class.</p>
      <p>Sensitive films are films which can be damaged easily on installation. These films include chrome, satin chrome, holographic chrome, brushed metal and carbon fiber. We will go through each film and the differences when it comes to handling and installation. By the time you finish our 2 day workshop you will have a much better understanding and much more confidence when it comes to installing sensitive films. This will open up a brand new market for you. Chrome wraps especially seem to be leading the way, and their popularity has vastly grown over a very short while. Many people want something that's "different", be sure you can offer your clients as many variations as possible.</p>
    </div>
  </div>
  <hr>
  <div class="row">
    <div class="col">
      <h2>Pricing</h2>
      <p>At our location, 43 Grenfell Crescent (Unit 4) Ottawa, ON K2G 0G3, Canada</p>
      <p>
        Level 1 &amp; 2 Workshops &#8212; $1,300 USD / $1,600 CAD – 3 day Workshop<br>
        Level 3 Workshop &#8212; $1,100 USD / $1,300 CAD – 2 day Workshop
      </p>

      <div class="row mt-2">
        <div class="col">
          <p class="text-center">
            <a class="btn btn-lg btn-success" href="https://www.vvividhq.com/get-certified" target="_blank" rel="noopener noreferrer">
              Get Certified Today!
            </a>
          </p>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection


@push('scripts')
<script>
  window.onload = () => {
    document.querySelector('.slides-control').addEventListener('click', () => {
      const top = document.querySelector('.scroll-target').offsetTop
      const offset = document.querySelector('.navbar').offsetHeight + 50
      window.scrollTo(0, top - offset)
    });
  }
</script>
@endpush