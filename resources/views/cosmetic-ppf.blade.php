@extends('layouts.app')

@section('top-content')

<div class="fullscreen">

  <div class="fullscreen">
    <div class="slide-mask">
        <div class="slide-content">
          <div class="container">
            <h1 class="slide-content-title">Cosmetic PPF Training</h1>
            <div class="slide-content-body">
                @include('partials.join_button')
            </div>
          </div>
        </div>
        <div class="slides-control">
           <div class="slides-control-down">
             Learn More
           </div>
         </div>
      </div>
    <img 
      class="cover"
     src="/img/cppf3.jpg" alt="Wrapped Lambo">
  </div>
</div>
@endsection

@section('content')
<div class="container mt-5 scroll-target">
  <div class="row">
    <div class="col-lg-5 background-images order-lg-last" style="background-image:url('/img/cppf4.jpg');background-size:cover"></div>
    <div class="col-lg-7">
      <h2>Personal Training Programs For Cosmetic PPF (Paint Protection Film)</h2>
      <p>Cosmetic PPF is installed dry like vinyl wrap, but there is a noticeable level of difficulty when it comes to tack and stretch. Though this film is much different than chrome, in my experience it can be just as difficult to maintain a clear and clean installation. Learn the best practices for dry PPF application and earn official FlexiShield factory certification. The in-person experience will not only save you time, but also the cost of damaged material. This is cutting edge technology which is becoming highly sought after by consumers. Set yourself apart and be one of very few who can currently offer and install this new type of film. </p>
      <p>You will learn about the tools and methods available to you during installation of FlexiShield Cosmetic Paint Protection Film. I will lead you through every step of the process to ensure you are equipped with the skills necessary to add cosmetic PPF to your expertise.</p>
      <p>CK Wraps is the first and only certified training facility in all of Canada. Find out what dates are available and register using Register Now button below.</p>
      <p>For PPF (paint protection film) certification training click <a href="{{route('ppf-training')}}">here</a></p>
      <p>For colour change certification training click <a href="{{route('training')}}">here</a></p>
    </div>
  </div>
  <hr>
  <div class="row">
    <div class="col">
      <h2>Pricing</h2>
      <p>At our location, 43 Grenfell Crescent (Unit 4) Ottawa, ON K2G 0G3, Canada</p>
      <p>
        Price $1,300 USD / $1,600 CAD – 3 day Workshop<br>
      </p>

      <div class="row mt-2">
        <div class="col">
          <p class="text-center">
        <a class="btn btn-lg btn-success" href="/contact-cppf">
          Register Now!
        </a>
          </p>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection


@push('scripts')
<script>
  window.onload = () => {
    document.querySelector('.slides-control').addEventListener('click', () => {
      const top = document.querySelector('.scroll-target').offsetTop
      const offset = document.querySelector('.navbar').offsetHeight + 50
      window.scrollTo(0, top - offset)
    });
  }
</script>
@endpush