@extends('layouts.app')


@section('content')
<div class="fullscreen" style="background:black;padding-bottom: 75px; overflow:visible !important; max-height: none; margin-bottom: -2rem">
  <div class="container" style=" color: white; padding-top:100px">
    <div class="row">
      <div class="col-md-12">
        <h1 class="text-center mt-5" style="font-size: 3.5rem">Advertise with CK Wraps!</h1>
        <i class="fa fa-youtube my-4 mx-auto" style="font-size:12rem; color:#ff0000; text-align:center;display:block" aria-hidden="true"></i>
        <h2 class="text-center mt-5" style="font-size: 2.25rem">Great reach for males age 25-44!</h2>
        <div class="row mt-5">
          <div class="col-md-4 text-center mb-3">
            <i class="fa fa-users mb-3" style="font-size:8rem;" aria-hidden="true"></i>
            <h2>Over <span id="subs">357,000<span><br><span class="small">subscribers</span></h2>
          </div>
          <div class="col-md-4 text-center mb-3">
            <i class="fa fa-line-chart mb-3" style="font-size:8rem;" aria-hidden="true"></i>
            <h2>Over <span id="views">1,200,000</span><br><span class="small">views per month</span></h2>
          </div>
          <div class="col-md-4 text-center mb-3">
            <i class="fa fa-clock-o mb-3" style="font-size:8rem;" aria-hidden="true"></i>
            <h2>Over <span id="hours">160,000</span><br><span class="small">hours monthly watch time</span></h2>
          </div>
        </div>
      </div>
      <div class="col-md-12 mt-5">
        <h2 class="text-center mb-5">YouTube Age Range</h2>
        <canvas class="pb-5" id="myChart"></canvas>
        <h2 class="mt-4">Work With Us</h2>
        <p style="font-size: 1.5rem">CK Wraps specialises in creating engaging content related to vinyl wrapping with a detailed breakdown of what is involved when it comes to wrapping. Whether it’s a talent-led video, sponsorship or content creation we are able to fulfil your campaign objectives by connecting you to your target audience.</p>
        <p class="h1 text-center"><a href="/contact">Click to conact us today about our advertising opportunities!</a></p>
      </div>
    </div>
  </div>
</div>

@endsection

@push('scripts')
 <script src="https://cdnjs.cloudflare.com/ajax/libs/counterup2/2.0.2/index.js"></script>
 <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

  <script>
  let counterUp = window.counterUp['default'];
  const subs = document.getElementById( 'subs' )
  const views = document.getElementById( 'views' )
  const hours = document.getElementById( 'hours' )

  // Start counting, do this on DOM ready or with Waypoints.
  counterUp( subs, {
      duration: 2000,
      delay: 10,
  } )
  counterUp( views, {
      duration: 2000,
      delay: 10,
  } )
  counterUp( hours, {
      duration: 2000,
      delay: 10,
  } )

  //age chart

    const labels = [
    '13-17',
    '18-24',
    '25-34',
    '35-54',
    '55-64',
    '64+',
  ];

  const data = {
    labels: labels,
    datasets: [{
      label: 'Percent of Users',
      backgroundColor: 'rgb(59,83,219)',
      borderColor: 'rgb(59,83,219)',
      data: [0.5,14.2,29.4,24.7,16.7,9.3,5],
    }]
  };

  const config = {
    type: 'bar',
    data: data,
    options: {
      responsive: true,
      scales: {
        y: {
          ticks: {color: 'white'}
        },
        x: {
          ticks: {color: 'white'}
        }
      }
    }
  };
  const myChart = new Chart(
    document.getElementById('myChart'),
    config
  );



  </script>
@endpush
