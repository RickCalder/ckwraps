@extends('layouts.app')

@section('top-content')

<div class="fullscreen">

  <div class="fullscreen">
    <div class="slide-mask">
        <div class="slide-content">
          <div class="container">
            <h1 class="slide-content-title">PPF Certification Workshop</h1>
            <div class="slide-content-body">
                @include('partials.join_button')
            </div>
          </div>
        </div>
        <div class="slides-control">
           <div class="slides-control-down">
             Learn More
           </div>
         </div>
      </div>
    <img 
      class="cover"
      srcset="/img/ppf-576.jpg 576w, /img/ppf-1024.jpg 1024w, /img/ppf-1920.jpg 1920w"
      sizes="(max-width: 576px) 576px,
      (max-width: 1024px) 1024px,
      (max-width: 1920px) 1920px"
     src="/img/ppf-1920.jpg" alt="PPF Wrap image">
  </div>
</div>
@endsection

@section('content')
<div class="container mt-5 scroll-target">
  <div class="row">
    <div class="col-lg-8 offset-lg-2 mb-4">
      <h2>Personal Training Programs For PPF (Paint Protection Films)</h2>
      <p>Training is very important in any industry. With the proper training, you'll be able to tackle many different obstacles. This training program is strictly for paint protection application. Color changes are what we've specialized in for 8 years.</p>
      <p>Our training is catered to your skill level. From a complete beginner who hasn't touched Paint Protection Film, to an advanced installer who would like to enhance their skills to another level. We can cover it all. Our Paint Protection Film process is thorough and in depth. We're here to answer all of your questions and help you to succeed.</p>
      <p>The new CK PROTECT PPF has been developed using cutting-edge technology and will protect your vehicle against external aggressions.</p>
      <ul>
        <li>Scratches</li>
        <li>Stone chips</li>
        <li>Abrasion</li>
        <li>UV rays</li>
      </ul>
      <h3 class="h5">Features</h3>
      <ul>
        <li><strong>Type of product:</strong> Protection film</li>
        <li><strong>Durability:</strong> Long term</li>
        <li><strong>Adhesive:</strong> Solvent-based acrylic</li>
        <li><strong>Type of adhesive:</strong> Solvent-based acrylic</li>
        <li><strong>Adhesive color:</strong> Transparent</li>
        <li><strong>Liner:</strong> PET silicone</li>
        <li><strong>Self-Healing:</strong> YES</li>
      </ul>
      <p>For colour change certification training click <a href="{{route('training')}}">here</a></p>
    </div>
  </div>
  <div class="row">
    <div class="col mb-4">
      <hr>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-4">
      <h2>Day One</h2>
      <h3 class="h5">Explanation & Demonstration</h3>
      <ul>
        <li>Safety</li>
        <li>Product qualities / limitations and ability <li>Technical information</li>
        <li>How it works</li>
        <li>Tools needed</li>
        <li>Material type, finishes and adhesive</li>
        <li>Clarity and durability</li>
        <li>Warranty</li>
        <li>Vehicle surface preparation</li>
        <li>What can and can not be wrapped</li>
        <li>Approaching a panel</li>
      </ul>
      <h3 class="h5">Hands On</h3>
      <ul>
        <li>Squeegee techniques, getting comfortable, handling of the film</li>
        <li>Fingering, wrinkling and wrinkles</li>
        <li>Reading the wrinkles / fingers</li>
        <li>Stretching, conforming and laying into recesses <li>Relaxing the film (edges), relief cuts</li>

      </ul>
    </div>
    <div class="col-lg-4">
      <h2>Day Two</h2>
      <ul>
        <li>Creating and driving business</li>
        <li>Corners and the different types. Different ways to wrap them <li>Cutting and trimming</li>
        <li>Scoring the film without cutting through it</li>
        <li>Bulk fender wrap</li>
        <li>Bulk hood wrap</li>
        <li>Seams / joints where to use them, how we do them and why? <li>Areas with a deep recess</li>
        <li>Damaged film during install. Is it recoverable or not?</li>
        <li>Horizontal and vertical surfaces</li>
        <li>Glue lines and adhesive particles</li>
        <li>Debris & dirt under the film, what to do</li>
        <li>Air bubbles and water bubbles</li>
        <li>Bulk mirror wrap</li>
        <li>Two piece mirror wrap</li>
        <li>Door handle wrap</li>

      </ul>
    </div>
    <div class="col-lg-4">
      <h3>Day Three</h3>
      <ul>
        <li>Using steam to aid in installation</li>
        <li>Self healing capabilities</li>
        <li>Headlights and taillights</li>
        <li>Bulk front bumper</li>
        <li>Bulk rear bumper</li>
      </ul>
      <h3 class="h5">Approximate time for this day is 6-8 hours</h3>
    </div>
  </div>
  <hr>
  <div class="row mt-3">
    <div class="col center">
      <h2>Location & Pricing</h2>
      <p>
        43 Grenfell Crescent (Unit 4) <br>
        Ottawa, ON K2G 0G3<br>
        Canada<br>
        <a href="https://www.google.com/maps/place/43+Grenfell+Crescent+unit+4,+Nepean,+ON+K2G+0G3/@45.3216451,-75.7245152,17z/data=!3m1!4b1!4m5!3m4!1s0x4cce0792c8855555:0x22f73b952bfc74fa!8m2!3d45.3216413!4d-75.7223265" target="_blamk">Map</a>
      </p>
      <p>
        Price: $1,300 USD / $1,600 CAD – 3 day Workshop
      </p>
    </div>
  </div>
  <div class="row mt-3">
    <div class="col">
      <p class="text-center">
        <a class="btn btn-lg btn-success" href="/contact-ppf">
          Get PPF Certified!
        </a>
      </p>
    </div>
  </div>
  <hr>
</div>
@endsection


@push('scripts')
<script>
  window.onload = () => {
    document.querySelector('.slides-control').addEventListener('click', () => {
      const top = document.querySelector('.scroll-target').offsetTop
      const offset = document.querySelector('.navbar').offsetHeight + 50
      window.scrollTo(0, top - offset)
    });
  }
</script>
@endpush