@extends('layouts.app')


@section('content')
<div class="container" style="margin-top: 100px">
  <div class="row">
    <div class="col-lg-12">
      <h1 class="mb-2">{{ $video->title }}</h1>
      {!! $video->description !!}
      <p><a href="/videos">Back to Video List</a></p>
    </div>
    @if($video->source === 'vimeo')
    <div class="col-lg-12">
        <div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/{{$video->video_id}}?quality=1080p" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>
    </div>
    @else
    <div class="col-lg-12">
        <div style="padding:56.25% 0 0 0;position:relative;">
          <iframe style="position:absolute;top:0;left:0;width:100%;height:100%;" src="https://www.youtube.com/embed/{{$video->video_id}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
    </div>
    @endif
    <div class="col-12">
      <div class="row mt-4" id="video">
        <div class="col-12 col-xl-8">
          <h2 class="mb-3">Comments</h2>
          {{Form::open(['action' => 'VideoController@comment'])}}
          @csrf
          <input type="hidden" name="video_id" value="{{$video->id}}"/>
          <input type="hidden" name="quoted" id="quoted"/>
            <div class="form-group">
              <textarea class="reply-text form-control" name="comment" id="comment" required></textarea>
            </div>
            <div class="form-group">
            <input type="submit" class="btn btn-sm btn-primary" value="Comment" />
            </div>
          </form>
          @foreach($comments as $comment)
          @include('./components/video-comment')
          @endforeach
        </div>
        <div class="col-12 col-xl-4">
          <h2 class="mb-3">Recommendations</h2>
          @foreach($recommendations as $rec)
          @include('./components/video-rec')
          @endforeach
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@push('scripts')
  <script>
    $(document).on('click', '.reply', function() {
      $('.reply').html('Reply')
      $('.reply-text').focus()
      $(this).html('Replying To')
      $('#quoted').val($(this).attr('data-commentid'))
    })
  </script>
@endpush