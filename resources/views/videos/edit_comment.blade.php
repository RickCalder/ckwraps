@extends('layouts.app')

@section('extra-head')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.19.1/ui/trumbowyg.min.css"
  integrity="sha256-iS3knajmo8cvwnS0yrVDpNnCboUEwZMJ6mVBEW1VcSA=" crossorigin="anonymous" />
@endsection

@section('content')
<div class="container" style="margin-top: 100px">
  <div class="row">
    <div class="col-lg-12">
      <p class="forum-breadcrumbs">
        <a href="{{ route('forums') }}">{{ __('Forums') }}</a> &nbsp; > Editing reply to - {{ $video->title }} 
      </p>
    </div>
  </div>
  <div class="row justify-content-center">
    <div class="col-lg-12 mt-4">
      @csrf
      {{Form::open(['action' => ['VideoController@update_comment', $comment->id], 'method' => 'PUT', 'id' => 'forum-form'])}}
      
       {{-- <form method="PUT" action="/forums/{{$comment->id}}/update_reply" accept-charset="UTF-8"> --}}
      <input hidden="hidden" name="video_id" value="{{ $video->id }}">
      <input hidden="hidden" name="comment_id" value="{{ $comment->id }}">
      <div class="form-group">
        <label for="body">Comment</label>
        <div class="form-group">
          <textarea class="reply-text form-control" name="comment" id="comment" required>{{ $comment->comment }}</textarea>
        </div>
      </div>
      @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div><br />
      @endif
      <div class="form-group">
        <button class="btn btn-primary" id="submit-thread" type="submit">Update</button>
      </div>
      </form>
    </div>
  </div>
  </container>
  @endsection

  @push('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.25.1/trumbowyg.min.js" crossorigin="anonymous">
  </script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.25.1/plugins/cleanpaste/trumbowyg.cleanpaste.min.js"
    crossorigin="anonymous"></script>
  <script src="https://upload-widget.cloudinary.com/global/all.js" type="text/javascript"></script>
  <script>
    $(document).ready(function(){ 
      $.noConflict()
    })

    $('#forum-form').submit(function(e) {
      e.preventDefault()
        $('#forum-form')[0].submit()
      }
    })

  </script>
  @endpush