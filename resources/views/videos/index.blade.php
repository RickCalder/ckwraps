@extends('layouts.app')

@section('top-content')

{{-- <div class="fullscreen">
  <div class="slide-mask">
    <div class="slides-control">
      <div class="slides-control-down">
        Learn More
      </div>
    </div>
  </div>
  <picture class="cover">
    <source class="cover" media="(max-width: 1023px)" srcset="/img/video-header-576.jpg">
    <source class="cover" media="(max-width: 1440px)" srcset="/img/video-header-1024.jpg">
    <img class="cover" src="/img/video-header.jpg">
  </picture>
</div> --}}
@endsection

@section('content')
@php
$allowed = \Request::get('userAllowed');

// dd($allowed);
@endphp
<div class="container scroll-target" id="video" style="margin-top: 100px">
  <div class="row">
    <div class="col-lg-12 d-flex justify-content-between">
      <h1 class="mb-3">Video Courses</h1>
    </div>
    <div class="col-lg-12 d-xl-none">
      <form action="{{ route('video-search') }}" class="form-inline mb-5" method="POST">
        @csrf
        <label for="searchinput" class="sr-only">Search Videos</label>
        <input type="text" class="form-control mr-3" id="searchinput" name="query" value="{{$searchTerm}}"
          placeholder="Search" />
        <label for="cat-select" class="mr-3 sr-only">Category:</label>
        <select class="form-control mr-3" style="min-width:200px" id="cat-select" name="category">
          <option value="0" {{$category === "0" ? 'selected' : ''}}>All Categories</option>
          <option value="4" {{$category === "20" ? 'selected' : ''}}>Getting Started</option>
          <option value="16" {{$category === "16" ? 'selected' : ''}}>Beginner 101</option>
          <option value="18" {{$category === "18" ? 'selected' : ''}}>Business 101</option>
          <option value="4" {{$category === "4" ? 'selected' : ''}}>Advanced</option>
          <option value="8" {{$category === "8" ? 'selected' : ''}}>Bumpers</option>
          <option value="2" {{$category === "2" ? 'selected' : ''}}>Disassembly</option>
          <option value="15" {{$category === "15" ? 'selected' : ''}}>Doors</option>
          <option value="7" {{$category === "7" ? 'selected' : ''}}>Door Handles</option>
          <option value="11" {{$category === "11" ? 'selected' : ''}}>Fenders</option>
          <option value="13" {{$category === "13" ? 'selected' : ''}}>Headlight/Taillight Tints</option>
          <option value="6" {{$category === "6" ? 'selected' : ''}}>Hoods</option>
          <option value="3" {{$category === "3" ? 'selected' : ''}}>Installation</option>
          <option value="14" {{$category === "14" ? 'selected' : ''}}>Interior</option>
          <option value="19" {{$category === "19" ? 'selected' : ''}}>Level 2 Workshop Program</option>
          <option value="10" {{$category === "10" ? 'selected' : ''}}>Mirrors</option>
          <option value="17" {{$category === "17" ? 'selected' : ''}}>PPF</option>
          <option value="9" {{$category === "9" ? 'selected' : ''}}>Roofs</option>
          <option value="5" {{$category === "5" ? 'selected' : ''}}>Techniques</option>
          <option value="12" {{$category === "12" ? 'selected' : ''}}>Trunks</option>
        </select>
        <input type="submit" class="btn btn-md btn-primary mr-5" value="Search" />
        <a href="/videos" class="btn btn-md btn-primary mr-2">View All Videos</a>
      </form>
    </div>
  </div>
  <div class="row">
    @if(!$allowed)
    <div class="col-lg-12">
      <div class="alert alert-danger">
        Our video courses are for premium members only, please register as a paid member or upgrade your account to gain
        access!
      </div>
    </div>
    @endif
  </div>
  <div class="row">
  <div class="d-none d-xl-block col-xl-3 mb-3">
    <div class="forum-sidebar">
      <h2 class="h3">Search</h2>
      <div>
        
        <form action="{{ route('video-search') }}" class="form-inline mb-3" method="POST">
          @csrf
          <label for="searchinput" class="sr-only">Search Videos</label>
          <input type="text" class="form-control mr-3 mb-2" id="searchinput" name="query" value="{{$searchTerm}}"
            placeholder="Search" />
          <label for="cat-select" class="mr-3 sr-only">Category:</label>
          <select class="form-control mr-3" style="max-width:200px" id="cat-select" name="category">
            <option value="0" {{$category === "0" ? 'selected' : ''}}>All Categories</option>
            <option value="20" {{$category === "20" ? 'selected' : ''}}>Getting Started</option>
            <option value="16" {{$category === "16" ? 'selected' : ''}}>Beginner 101</option>
            <option value="18" {{$category === "18" ? 'selected' : ''}}>Business 101</option>
            <option value="4" {{$category === "4" ? 'selected' : ''}}>Advanced</option>
            <option value="8" {{$category === "8" ? 'selected' : ''}}>Bumpers</option>
            <option value="2" {{$category === "2" ? 'selected' : ''}}>Disassembly</option>
            <option value="15" {{$category === "15" ? 'selected' : ''}}>Doors</option>
            <option value="7" {{$category === "7" ? 'selected' : ''}}>Door Handles</option>
            <option value="11" {{$category === "11" ? 'selected' : ''}}>Fenders</option>
            {{-- <option value="1" {{$category === "1" ? 'selected' : ''}}>General</option> --}}
            <option value="13" {{$category === "13" ? 'selected' : ''}}>Headlight/Taillight Tints</option>
            <option value="6" {{$category === "6" ? 'selected' : ''}}>Hoods</option>
            <option value="3" {{$category === "3" ? 'selected' : ''}}>Installation</option>
            <option value="14" {{$category === "14" ? 'selected' : ''}}>Interior</option>
            <option value="19" {{$category === "19" ? 'selected' : ''}}>Level 2 Workshop Program</option>
            <option value="10" {{$category === "10" ? 'selected' : ''}}>Mirrors</option>
            <option value="17" {{$category === "17" ? 'selected' : ''}}>PPF</option>
            <option value="9" {{$category === "9" ? 'selected' : ''}}>Roofs</option>
            <option value="5" {{$category === "5" ? 'selected' : ''}}>Techniques</option>
            <option value="12" {{$category === "12" ? 'selected' : ''}}>Trunks</option>
          </select>
          <input type="submit" class="btn btn-md btn-primary mr-5" value="Search" />
        </form>
      </div>
      <h2 class="h3">Categories</h2>
      <ul class="forum-tags">
        <li>
          <a href="/videos"
            class="{{ Request::is('videos') ? 'active' : '' }}">
            All
          </a>
          <a href="/videos/cats/20"
            class="{{ Request::is('videos/cats/20') ? 'active' : '' }}">
            Getting Started
          </a>
          <a href="/videos/cats/16"
            class="{{ Request::is('videos/cats/16') ? 'active' : '' }}">
            Beginner 101
          </a>
          <a href="/videos/cats/18"
            class="{{ Request::is('videos/cats/18') ? 'active' : '' }}">
            Business 101
          </a>
          <a href="/videos/cats/4"
            class="{{ Request::is('videos/cats/4') ? 'active' : '' }}">
            Advanced
          </a>
          <a href="/videos/cats/8"
            class="{{ Request::is('videos/cats/8') ? 'active' : '' }}">
            Bumpers
          </a>
          <a href="/videos/cats/2"
            class="{{ Request::is('videos/cats/2') ? 'active' : '' }}">
            Disassembly
          </a>
          <a href="/videos/cats/15"
            class="{{ Request::is('videos/cats/15') ? 'active' : '' }}">
            Doors
          </a>
          <a href="/videos/cats/7"
            class="{{ Request::is('videos/cats/7') ? 'active' : '' }}">
            Door Handles
          </a>
          <a href="/videos/cats/11"
            class="{{ Request::is('videos/cats/11') ? 'active' : '' }}">
            Fenders
          </a>
          <a href="/videos/cats/13"
            class="{{ Request::is('videos/cats/13') ? 'active' : '' }}">
            Headlight/Taillight Tints
          </a>
          <a href="/videos/cats/6"
            class="{{ Request::is('videos/cats/6') ? 'active' : '' }}">
            Hoods
          </a>
          <a href="/videos/cats/3"
            class="{{ Request::is('videos/cats/3') ? 'active' : '' }}">
            Installation
          </a>
          <a href="/videos/cats/14"
            class="{{ Request::is('videos/cats/14') ? 'active' : '' }}">
            Interior
          </a>
          <a href="/videos/cats/19"
            class="{{ Request::is('videos/cats/19') ? 'active' : '' }}">
            Level 2 Workshop Program
          </a>
          <a href="/videos/cats/10"
            class="{{ Request::is('videos/cats/10') ? 'active' : '' }}">
            Mirrors
          </a>
          <a href="/videos/cats/17"
            class="{{ Request::is('videos/cats/17') ? 'active' : '' }}">
            PPF
          </a>
          <a href="/videos/cats/9"
            class="{{ Request::is('videos/cats/9') ? 'active' : '' }}">
            Roofs
          </a>
          <a href="/videos/cats/5"
            class="{{ Request::is('videos/cats/5') ? 'active' : '' }}">
            Techniques
          </a>
          <a href="/videos/cats/12"
            class="{{ Request::is('videos/cats/12') ? 'active' : '' }}">
            Trunks
          </a>
        </li>
      </ul>
    </div>
  </div>
  <div class="col-xl-9">
  <div class="row row-eq-height">
    @if(isset($videos) && count($videos) > 0)
    @foreach ($videos as $video)
    <div class="col-lg-4 mb-4">
      <div>          
        @if($allowed)
        <a href="/videos/{{$video->id}}">
        @endif
        <div class="mb-3">
          @if($video->thumbnail !== null)
          <img src="{{Storage::url($video->thumbnail)}}"  class="card-img-top">
          @else
          @if($video->source === 'vimeo')
          <VideoThumbnail url="{{$video->private_link}}" />
            @else
            <img src="https://img.youtube.com/vi/{{$video->video_id}}/0.jpg"  class="card-img-top">
            @endif
          @endif
        </div>
        <h2 class="h5 text-dark">
          {{ $video->title }}
        </h2>
        @if($loop->index <= 2)
        <span class="badge badge-success" style="font-size: 1.25rem">New</span>
        @endif
        @if($allowed)
        @endif
        <p class="text-dark">{{ \Illuminate\Support\Str::limit(strip_tags($video->description), 95, $end='...') }}</p>
      </a>
    </div>
    </div>
    @endforeach
    @else
    <div class="col-lg-12 mt-4">
      <p class="h2">
        There were no videos matching the search "{{$searchTerm}}" please search again or view all <a
          href="/videos">videos</a>
      </p>
    </div>

    @endif
  </div>
</div>
</div>
  @if(isset($videos))
  {{ $videos->onEachSide(1)->links() }}
  @endif
</div>

@endsection

@push('scripts')
<script>
  // window.onload = () => {
  //   document.querySelector('.slides-control').addEventListener('click', () => {
  //     const top = document.querySelector('.scroll-target').offsetTop
  //     const offset = document.querySelector('.navbar').offsetHeight + 50
  //     window.scrollTo(0, top - offset)
  //   });
  //   $('select').on('change', function() {
  //     console.log( this.value );
  //   });
  //   if(window.location.pathname === '/search') {
  //     const top = document.querySelector('.scroll-target').offsetTop
  //     const offset = document.querySelector('.navbar').offsetHeight + 50
  //     window.scrollTo(0, top - offset)
  //   }
  // }
</script>
<script>
</script>
@endpush