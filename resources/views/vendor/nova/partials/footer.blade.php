<p class="mt-8 text-center text-xs text-80">
    CK Wraps
    <span class="px-1">&middot;</span>
    &copy; {{ date('Y') }} <a href="https://calder.io" target="_blank">calder.io</a>
    <span class="px-1">&middot;</span>
    v{{ Laravel\Nova\Nova::version() }}
</p>
