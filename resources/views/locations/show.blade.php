@extends('layouts.app')


@section('content')
@php 
$website = strtolower($location->website);
  if( substr($website, 0, 4) !== 'http') {
    $website = '';
  }
@endphp
<div class="container" style="margin-top: 100px">
  <div class="row">
    <div class="col-md-12">
      <a href="/locations">Back to Location Board</a>
      <h1 class="my-3">{{$location->name}}</h1>
      <p>@if($website !== '')Website: <a href="{{$website}}" target="_blank">{{$website}}</a> | @endif Phone: <a
          href="tel:{{$location->phone}}">{{$location->phone}}</a> | Email: <a href="mailto:{{$location->email}}"
          target="_blank">{{$location->email}}</a>
      </p>
      @php
          $mapAddress = str_replace(' ', '+', $location->address);
      @endphp
      <p>{{$location->address}} <a href="https://www.google.ca/maps/place/{{$mapAddress}}" target="_blank">Map</a></p>
      <div>
        {!! $location->services !!}
      </div>
    </div>
  </div>
</div>
@endsection