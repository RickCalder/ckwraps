@extends('layouts.app')


@section('content')
<div class="container" style="margin-top: 100px">
  <div class="row">
    <div class="col-md-12">
      <h1 class="mb-3">Location Board</h1>
      @if( count($locations) === 0 )
      <p>There are no businesses listed yet, please check back soon!</p>
      @else
      <div class="ml-4">
        @foreach( $locations as $location )
        @php 
        $website = strtolower($location->website);
          if( substr($website, 0, 4) !== 'http') {
            $website = '';
          }
        @endphp
        <div>
          <h2>{{ $location->name }}</h2>
          <p>@if($website !== '')Website: <a href="{{$website}}" target="_blank">{{$website}}</a> | @endif Phone: <a
              href="tel:{{$location->phone}}">{{$location->phone}}</a> | Email: <a href="mailto:{{$location->email}}"
              target="_blank">{{$location->email}}</a>
          </p>
          <div class="text-muted">
            {{ str_limit(strip_tags($location->services), $limit = 100, $end = '...') }}
          </div>
          @php
              $mapAddress = str_replace(' ', '+', $location->address);
          @endphp
          <p>{{$location->address}} <a href="https://www.google.ca/maps/place/{{$mapAddress}}" target="_blank">Map</a></p>
          <p class="mt-3"><a href="/locations/{{$location->id}}">Read More...</a></p>
        </div>
        <hr>
        @endforeach
      </div>
      {{ $locations->links() }}
      @endif
    </div>
  </div>
</div>
@endsection