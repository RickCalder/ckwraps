@extends('layouts.app')

@section('top-content')

@endsection

@section('content')
@php
$allowed = \Request::get('userAllowed');
@endphp
<div class="container scroll-target" id="video" style="margin-top: 100px">
  <div class="row">
    <div class="col-lg-12 d-flex justify-content-between">
      <h1 class="mb-3">Live Events</h1>
    </div>
  </div>
  <div class="row">
    @if(!$allowed)
    <div class="col-lg-12">
      <div class="alert alert-danger">
        Our live events are for premium members only, please register as a paid member or upgrade your account to gain
        access!
      </div>
    </div>
    @endif
  </div>
  @if($allowed)
  <div class="row">
  <div class="col-xl-12">
    @if(isset($events) && count($events) > 0)
    <h2>{{$events[0]->title}}</h2>
    <p>{{$events[0]->description}}</p>
  </div>
  <div class="col-xl-9">
    {!! $events[0]->video !!}
  </div>
  <div class="col-xl-3" style="min-height: 600px">
    {!! $events[0]->chat !!}
  </div>
    @else
    <div class="col-lg-12 mt-4">
      <p class="h2">
        There are no current live events scheduled. Please check back soon!
      </p>
    </div>

    @endif
  </div>
  @endif
  <!--
  @if(isset($events) && count($events) > 0)
  <h2 class="mt-4">Past Events</h2>
  <div class="row row-eq-height mt-4">
    @foreach($events as $event)
      <div class="col-xl-4">
        {!! $events[0]->video !!}
        <h2 class="mt-2">{{$events[0]->title}}</h2>
        <p>{{$events[0]->description}}</p>
      </div>
    @endforeach
  </div>
  @endif
</div>
  @if(isset($events))
  {{ $events->onEachSide(1)->links() }}
  @endif
</div>
-->
@endsection

@push('scripts')
@endpush