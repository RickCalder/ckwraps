@extends('layouts.app')

@section('top-content')

<div class="fullscreen">
  <div class="slide-mask">
      <div class="slide-content">
        <div class="container">
          <img src={{ url('/img/ckwraps-logo.png')}} alt="CKWraps logo home link" style="max-width: 80%;"/>
          <h1 class="slide-content-title">The Wrap Channel</h1>
          <div class="slide-content-body">
            <p>Professional Vinyl Wrapping, Certified Training and online video courses!</p>
            @include('partials.join_button')
          </div>
        </div>
      </div>
      <div class="slides-control">
         <div class="slides-control-down">
           Learn More
         </div>
       </div>
    </div>
  <img    
    style="object-position: center ; object-fit: cover;"
    class="cover-with-video"
    src="/img/audi-church-phone.jpg" alt="Blue Audi">
  <video id="headervideo"  playsinline autoplay="autoplay" muted="muted" loop="loop" style="min-width: 100%;max-height: 100vw" poster="/img/audi-church-1920.jpg" data-videosrc="https://player.vimeo.com/external/540864119.sd.mp4?s=16827894414b1e40a0e33ed0ede4f23f427dd9b8&profile_id=165">
    <source type="video/mp4">
  </video>
</div>
@endsection

@section('content')

@include('./components/video-counter')

<div class="container mt-5 scroll-target">
  <div class="row mb-5">
    <div class="col-lg-4 background-images order-lg-last" style="background-image:url('/img/lambo-576.jpg');background-size:cover"></div>
    <div class="col-lg-8">
      <h2>About Us</h2>
      <p>Our website will arm you with the right tools to become a seasoned pro in custom vehicle wrapping. We help you gain self-sufficiency and knowledge through: personalized support, online video training in 4K resolution, exclusive resources, location boards, and communication boards for troubleshooting. You will also be able to showcase your business through our platform, which can help you gain exposure and clientele. Website members will have exclusive access to all of our valuable content, which is consistently being updated.</p>
    </div>
  </div>
  <div class="row lg-5">
    <div class="col-md-4 background-images" style="background-image:url('/img/certified-training.jpg');background-size:cover"></div>
    <div class="col-lg-8">
      <h2>Certified Training</h2>
      <p>What helps CK Wraps stand out is the passion and artistry that we bring to every project. Given our extensive experience, our application experts collected pivotal training and expertise for the installation and removal of vinyl wraps & <a href="https://www.vinylwraptraining.com/services" target="_blank">Paint Protection Films</a> from any surface type.</p>
      <p>We also serve as <a href="https://www.vinylwraptraining.com/" target="_blank">{{ __('certified trainers') }}</a> for various manufacturers in Ottawa, Canada. We specialize in the application of sensitive materials such as chrome, effect, Paint Protection Films and texture wraps, which led us to become the main training hub in Ontario for these hard-to-install materials.</p>
    </div>
  </div>
</div>
@endsection

@push('scripts')
<script>
  window.onload = () => {
    document.querySelector('.slides-control').addEventListener('click', () => {
      const top = document.querySelector('.scroll-target').offsetTop
      const offset = document.querySelector('.navbar').offsetHeight + 50
      window.scrollTo(0, top - offset)
    });

    const width = window.outerWidth
    const source = document.querySelectorAll('#headervideo source')
    const video = document.getElementById('headervideo')
    if(width >= 768) {
      source[0].setAttribute('src', video.getAttribute('data-videosrc'))
      video.load()
    } else {
      console.log('boo')
    }
  }
</script>
@endpush
