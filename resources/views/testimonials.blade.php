@extends('layouts.app')


@section('content')
<div class="container" style="margin-top: 100px">
  <div class="row">
    <div class="col-md-12">
      <h1 class="mb-5">CK Wraps Premium Member Testimonials</h1>
    </div>
  </div>
  @if(count($reviews) === 0)
  <h2>There are no testimonials at the moment, please check back later!</h2>
  @if(auth()->user()->role_id !== 3)
  <p>You can create a review or recommend Ck Wraps from your <a href="/my-account/review">account controls</a></p>
  @endif
  @else
  <div class="testimonials">
    @foreach ($reviews as $review)
      <blockquote>
        {!! nl2br(e($review->body)) !!}
      <span>~ {{$review->name}}</span>
      </blockquote>
    @endforeach
  </div>
  @endif
  {{ $reviews->links() }}
</div>

@endsection