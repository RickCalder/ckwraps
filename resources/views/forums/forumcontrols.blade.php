
@auth

@if( auth()->user()->id === $thread->user_id || auth()->user()->role_id <= 2)

<div class="d-flex align-items-center">
  {{ Form::open(['route' => ['threads.like'], 'method' => 'push', 'id' => 'like-form']) }}
  {{ csrf_field() }}
  <input name="thread" type="hidden" value="{{$thread->id}}" />
  <input name="type" type="hidden" value="0" />
    <button id="like-thread" type="submit" title="Like this thread" class="{{$liked}} like-button" style="border:none">
      <i class="fa fa-thumbs-up"></i>
      <span style="font-size:1rem">Like</span>
      <span class="sr-only">Like this thread</span>
      <span style="font-size: 0.85rem; color: #aaa !important">
      (<span class="like-count">{{$likes->count()}}</span>)
    </span>
    </button>
      
    {{ Form::close() }}
    {{ Form::open(['route' => ['threads.delete', $thread->id], 'method' => 'delete', 'id' => 'delete-form']) }}
      @csrf
      <button id="delete-thread" class="ml-3"><i class="fa fa-trash text-danger" title="Delete this thread"></i><span class="sr-only">Delete this thread</span></button>
    {{ Form::close() }}
    {{ Form::open(['route' => ['threads.edit', $thread->slug], 'method' => 'get']) }}
      @csrf
      <button type="submit" class="ml-3"><i class="fa fa-edit" title="Edit this thread"></i><span class="sr-only">Edit this thread</span></button>
    {{ Form::close() }}
</div>
@endif
@endauth
@push('scripts')
<script>
  let likes = {{$likes->count()}}
  $('#like-thread').on('click', function(e) {
    e.preventDefault()
    if($(this).hasClass('nolike')) {
      $(this).removeClass('nolike').addClass('like')
      likes = parseInt(likes)  +1
    console.log(likes)
    } else {
      $(this).removeClass('like').addClass('nolike')
      likes = parseInt(likes) -1
    }
      $('.like-count').html(likes)
    $.ajax({
      type: "POST",
      url: '/forums/like',
      data: $('#like-form').serialize(), 
      success: function( msg ) {
      }
    });
  })
</script>
@endpush