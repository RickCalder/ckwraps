@extends('layouts.app')


@section('content')
<div class="container" style="margin-top: 100px">
  <div class="row">
    <div class="col-md-12">
      @if( isset($threads[0]) )
        <h1 class="h2">Threads I have replied to</h1>
      @else 
        <h1 class="h2">Discussion Forum</h1>
      @endif
    </div>
  </div>
  @if($allowed === '0')
  <div class="row mt-4">
    <div class="col-md-12">
      <div class="alert alert-danger">
        Our Discussion forums are for members only. To gain access please <a
          href="{{ route('register') }}">{{ __('Register') }}</a> an account or <a
          href="{{ route('login') }}">{{ __('Login') }}</a>
      </div>
    </div>
  </div>
  @endif
  <div class="row mt-4">
    <div class="col-lg-3">
      <div class="forum-sidebar">
        @auth
        <a href="/forums/create-thread" class="btn btn-primary d-block mb-3">Create New Thread</a>
        <form action="{{ route('forum-search') }}" class="form-inline mb-3" method="POST">
          @csrf
          <label for="searchinput" class="sr-only">Search Videos</label>
          <input type="text" class="form-control mr-3" id="searchinput" name="query" value="{{$searchTerm}}"
            placeholder="Search" required />
          <input type="submit" class="btn btn-md btn-primary" value="Search" />
        </form>
        <h2 class="h3">My Links</h2>
        <ul class="forum-tags">
          <li>
            <a href="/forums/my-threads" class="{{ Request::is('forums/my-threads') ? 'active' : '' }}"><strong>My Threads</strong></a>
          </li>
          <li>
            <a href="/forums/my-replies" class="{{ Request::is('forums/my-replies') ? 'active' : '' }}"><strong>My Replies</strong></a>
          </li>
        </ul>
        @endauth
        <h2 class="h3">Tags</h2>
        <ul class="forum-tags">
          <li>
            <a href="{{ route('forums') }}" class="{{ Request::is('forums') ? 'active' : '' }}">
              {{ __('All') }}
            </a>
          </li>
          @foreach($tags as $tag)
          <li>
            <a href="/forums/tags/{{ $tag->tag_slug }}"
              class="{{ Request::is('forums/tags/'.$tag->tag_slug) ? 'active' : '' }}">
              {{$tag->tag_name}}
            </a>
          </li>
          @endforeach
        </ul>
      </div>
    </div>
    <div class="col-lg-9 forum-body justify-content-center">
      @if($threads->count() === 0)
      <p class="h4">There are no threads created by this user yet, check back later! @auth <a
          href="{{ route('forums') }}">Back to forums</a> @endauth</p>
      @endif
      @foreach($threads as $thread)
      @if($thread->thread !== null)
      <div class="card mb-3 {{ $thread->tag_slug }}">
        <div class="card-body">
            @if($allowed)
          <a class="forum-thread-link" href="{{ route('thread', $thread->thread->slug) }}">
            @endif
            <div class="d-flex justify-content-between">
              <h5 class="card-title d-flex mb-1"><strong>{{ $thread->thread->title }}</strong></h5>
            </div>
          <div class="card-text forum-excerpt">{{ str_limit(strip_tags($thread->thread->body), $limit = 100, $end = '...') }}.</div>
          @if($allowed)
        </a>
        @endif
        @if(isset($thread->thread->user))
        <p class="d-flex h6 mt-3">
          <a href="/forums/user/{{$thread->thread->user->id}}">{{ $thread->thread->user->name }}</a>&nbsp posted
          {{ $thread->thread->created_at->diffForHumans() }}</p>
          @endif
        </div>
      </div>
      @endif
      @endforeach
      {{ $threads->links() }}
    </div>
  </div>
</div>

@endsection