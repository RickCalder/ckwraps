
@if($thread->user->avatar)
<div>
<img src="{{ Storage::url('/avatars/'.$thread->user->avatar->image) }}" class="avatar avatar-sm" />
</div>
@else
<div class="avatar avatar-sm avatar-initials d-flex align-items-center justify-content-center">
  <span style="line-height:0">
    {{substr($thread->user->name,0,1)}}
  </span>
</div>
@endif