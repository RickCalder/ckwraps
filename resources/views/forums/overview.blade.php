@extends('layouts.app')


@section('content')
<div class="container" style="margin-top: 100px">
  <div class="row">
    <div class="col-md-12">
      <h1 class="h2">Discussion Forum</h1>
    </div>
  </div>
  @if(! $allowed)
  <div class="row mt-4">
    <div class="col-md-12">
      <div class="alert alert-danger">
        Our Discussion forums are for premium members only. To gain access please <a
          href="{{ route('register') }}">{{ __('Register') }}</a> an account or <a
          href="{{ route('login') }}">{{ __('Login') }}</a>
      </div>
    </div>
  </div>
  @endif
  <div class="row mt-4">
    <div class="col-lg-3">
      <div class="forum-sidebar">
        @if($allowed)
        <a href="/forums/create-thread" class="btn btn-primary d-block mb-3">Create New Thread</a>
        <form action="{{ route('forum-search') }}" class="form-inline mb-3" method="POST">
          @csrf
          <label for="searchinput" class="sr-only">Search Videos</label>
          <input type="text" class="form-control mr-3" id="searchinput" name="query" value="{{$searchTerm}}"
            placeholder="Search" />
          <input type="submit" class="btn btn-md btn-primary" value="Search" />
        </form>
        <h2 class="h3">My Links</h2>
        <ul class="forum-tags">
          <li>
            <a href="/forums/my-threads" class="{{ Request::is('forums/my-threads/') ? 'active' : '' }}"><strong>My
                Threads</strong></a>
          </li>
          <li>
            <a href="/forums/my-replies" class="{{ Request::is('forums/my-replies/') ? 'active' : '' }}"><strong>My
                Replies</strong></a>
          </li>
        </ul>
        @endif
        <h2 class="h3">Tags</h2>
        <ul class="forum-tags">
          <li>
            <a href="{{ route('forums') }}" class="{{ Request::is('forums') ? 'active' : '' }}">
              {{ __('All') }}
            </a>
          </li>
          @foreach($tags as $tag)
          @if($allowed)
          <li>
            <a href="/forums/tags/{{ $tag->tag_slug }}"
              class="{{ Request::is('forums/tags/'.$tag->tag_slug) ? 'active' : '' }}">
              {{$tag->tag_name}}
            </a>
          </li>
          @else
          <li>
            <a href="#" class="{{ Request::is('forums/tags/'.$tag->tag_slug) ? 'active' : '' }}">
              {{$tag->tag_name}}
            </a>
          </li>
          @endif

          @endforeach
        </ul>
      </div>
    </div>
    <div class="col-lg-9 forum-body justify-content-center">
      @if($current_tag !== null)
      <h2>{{$current_tag->tag_name}}</h2>
      @if($current_tag->description)
      <p>{{$current_tag->description}}</p>
      @endif
      <hr>
      @endif
      @if($threads->count() === 0)
      <p class="h4">There are no threads with this tag yet, check back later! @if($allowed === '1') <a
          href="{{ route('threads.create', ['slug' => Request::segment(3)]) }}">Why not create one?</a> @endif</p>
      @endif
      @foreach($announcements as $announcement)
      @include('partials.announcements')
      @endforeach
      @foreach($threads as $thread)
      <div class="card mb-3  @if($thread->announcement>=1)card-announcement @endif">
        <div class="card-body">
          <div class="d-flex">
            <div>
              @if($thread->user->avatar)
              <div>
                <img src="{{ Storage::url('/avatars/'.$thread->user->avatar->image) }}" class="avatar avatar-sm" />
              </div>
              @else
              <div class="avatar avatar-sm avatar-initials d-flex align-items-center justify-content-center">
                <span style="line-height:0">
                  {{substr($thread->user->name,0,1)}}
                </span>
              </div>
              @endif
            </div>
            <div class="ml-2 w-100">
              @if($allowed)
              <a class="forum-thread-link" href="{{ route('thread', $thread->slug) }}">
                @endif
                <div class="d-flex justify-content-between">
                  <h5 class="card-title d-flex mb-1"><strong>{{ $thread->title }}@if($thread->announcement>=1) -
                      Annoucement @endif</strong></h5>
                  <div class="d-flex">
                    <p class="d-flex h6 mr-3"><i class="fa fa-thumbs-up" aria-hidden="true"
                        style="color:#bbb">&nbsp;</i>
                      {{ count($thread->likes) }}</p>

                    <p class="d-flex h6"><i class="fa fa-comment" aria-hidden="true" style="color:#bbb">&nbsp;</i>
                      {{ count($thread->replies) }}</p>
                  </div>
                </div>
                <div class="card-text forum-excerpt">
                  @if($allowed)
                  {{ str_limit(strip_tags($thread->body), $limit = 100, $end = '...') }}
                  @else
                  <p>To view the contents of this thread you must be a premium member, either <a
                      href="{{route('login')}}">login</a> or <a href="{{route('register')}}">Join Now</a></p>
                  @endif
                </div>
                @if($allowed)
              </a>
              @endif

              @if($allowed && isset($thread->user))
              @php
              $Line ="";
              @endphp
              @if(isset($thread->tag))
              @php
              $line = 'in &nbsp; <a href="/forums/tags/' . $thread->tag->tag_slug .'">' . $thread->tag->tag_name .
                '</a>'

              @endphp
              @endif
              @if(count($thread->replies) === 0)
              <p class="h6 mt-3">
                <a href="/forums/user/{{$thread->user->id}}">{{ $thread->user->name }}</a>&nbsp; posted
                {{ $thread->created_at->diffForHumans() }} {!! $line !!}</p>
              @else
              <p class="h6 mt-3">
                <a href="/forums/user/{{$thread->user->id}}">{{ $thread->user->name }}</a>&nbsp; posted
                {{ $thread->created_at->diffForHumans() }} {!! $line !!}</p>
              <p class="h6">
                @php
                $reply_user = count($thread->replies) - 1;
                @endphp
                @if($thread->replies[$reply_user]->owner)
                <a
                  href="/forums/user/{{$thread->replies[0]->id}}">{{ $thread->replies[$reply_user]->owner->name }}</a>&nbsp;
                replied
                @endif
                {{ $thread->updated_at->diffForHumans() }}</p>
              @endif
              @else
              @if(isset($thread->user))
              @if(count($thread->replies) === 0)
              <p class="d-flex h6 mt-3">
                <a href="#">{{ $thread->user->name }}</a>&nbsp; posted
                {{ $thread->created_at->diffForHumans() }} in &nbsp;<a href="#">{{$thread->tag->tag_name}}</a></p>
              @else
              <p class="d-flex h6 mt-3">
                <a href="#">{{ $thread->user->name }}</a>&nbsp; posted
                {{ $thread->created_at->diffForHumans() }} in &nbsp;<a href="#">{{$thread->tag->tag_name}}</a></p>
              <p class="d-flex h6">
                @php
                $reply_user = count($thread->replies) - 1;
                @endphp
                @if($thread->replies[$reply_user]->owner->name)
                <a href="#">{{ $thread->replies[$reply_user]->owner->name }}</a>&nbsp; replied
                @endif
                {{ $thread->updated_at->diffForHumans() }}</p>
              @endif
              @endif
              @endif
            </div>
          </div>
        </div>
      </div>
      @endforeach
      {{ $threads->onEachSide(1)->links() }}
    </div>
  </div>
</div>

@endsection