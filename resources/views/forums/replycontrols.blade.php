

@php 
if(in_array( $reply->id, $my_likes)) {
  $liked = 'like';
}else{
  $liked = 'nolike';
}
@endphp
@auth 
@if( auth()->user()->id === $reply->owner_id || auth()->user()->role_id <= 2)

<div class="d-flex align-items-center">
    <button data-likes="{{$reply->likes->count()}}" data-replyid="{{$reply->id}}" type="submit" title="Like this reply" class="like-reply {{$liked}} like-button" style="border:none">
      <i class="fa fa-thumbs-up"></i>
      <span style="font-size:1rem">Like</span>
      <span class="sr-only">Like this reply</span>
      <span style="font-size: 0.85rem; color: #aaa !important">
      (<span class="like-count">{{$reply->likes->count()}}</span>)
    </span>
    </button>

    {{ Form::open(['route' => ['replies.delete', $reply->id], 'method' => 'delete', 'id' => 'delete-reply-form']) }}
      @csrf
      <button id="delete-reply" class="ml-3"><i class="fa fa-trash text-danger" title="Delete this reply"></i><span class="sr-only">Delete this reply</span></button>
    {{ Form::close() }}
    {{ Form::open(['route' => ['replies.edit_reply', $reply->id], 'method' => 'get']) }}
      @csrf
      <button type="submit" class="ml-3"><i class="fa fa-edit" title="Edit this reply"></i><span class="sr-only">Edit this reply</span></button>
    {{ Form::close() }}
</div>
@endif
@endauth
@push('scripts')
<script>
  $('.like-reply').on('click', function(e) {
    e.preventDefault()
    e.stopPropagation();
    e.stopImmediatePropagation();
    if($(this).hasClass('nolike')) {
      $(this).removeClass('nolike').addClass('like')
      
      $(this).closest('div').find('.like-count').html(parseInt($(this).data('likes'))  +1)
      $(this).data('likes', parseInt($(this).data('likes'))  +1)
    } else {
      $(this).removeClass('like').addClass('nolike')
      $(this).closest('div').find('.like-count').html(parseInt($(this).data('likes'))  -1)
      $(this).data('likes', parseInt($(this).data('likes'))  -1)
    }
    $.ajax({
      type: "POST",
      url: '/forums/like_reply',
      data: {reply: $(this).data('replyid')}, 
      success: function( msg ) {
      }
    });
  })
</script>
@endpush
