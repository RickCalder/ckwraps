@extends('layouts.app')

@section('extra-head')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.19.1/ui/trumbowyg.min.css"
  integrity="sha256-iS3knajmo8cvwnS0yrVDpNnCboUEwZMJ6mVBEW1VcSA=" crossorigin="anonymous" />

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css">
@endsection

@section('content')
<div class="container" style="margin-top: 100px">
  <div class="row">
    <div class="col-lg-12">
      <p class="forum-breadcrumbs">
        <a href="{{ route('forums') }}">{{ __('Forums') }}</a> &nbsp; > Editing - {{ $thread->title }} 
      </p>
    </div>
  </div>
  <div class="row justify-content-center">
    <div class="col-lg-8 mt-4">
      @csrf
      {{Form::open(['action' => ['ForumController@update_thread', $thread->slug], 'method' => 'PUT', 'id' => 'forum-form'])}}
    <input hidden="hidden" name="thread_id" value="{{ $thread->id }}">
      <div class="form-group">
        <label for="threadTag">Tag</label>
        <select name="tag" id="threadTag" class="form-control">
          <option value="">Select a Tag</option>
          @foreach($tags as $tag)
          <option 
            value="{{ $tag->id }}" 
            @if ($thread->tag_id == $tag->id) selected="selected") @endif
            @if (old('tag') == $tag->id) selected="selected") @endif
            
            >{{ $tag->tag_name }}</option>
          @endforeach
        </select>
      </div>
      @if( @auth()->user()->role_id === 1)
      <div class="form-group">
        <label for="announcement">Thread Type</label>
        <select name="announcement" id="announcement" class="form-control">
          <option value="0" @if($thread->announcement === 0) selected="selected" @endif>Normal Thread</option>
          <option value="2" @if($thread->announcement === 2) selected="selected" @endif>Announcement</option>
          <option value="1" @if($thread->announcement === 1) selected="selected" @endif>Vote</option>
        </select>
      </div>
      <div class="form-group">
          <label for="vote-expiry">Vote Expires <small> - (Only edit if this is a vote thread)</small></label>
        <input type='text' class="form-control" name="vote_expiry" id="vote-expiry" value="{{$thread->vote_expiry}}" />
      </div>
      @else 
      <input type="hidden" name="announcement" value="0" />
      @endif
      <div class="form-group">
        <label for="title">Title</label>
        <input type="text" class="form-control" name="title" id="title" value="{{ $thread->title }}" maxlength="100">
      </div>
      <div class="form-group">
        <label for="body">Body</label>
        <textarea id="body" name="body">
            {{ $thread->body }}
        </textarea>
      </div>
      @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div><br />
      @endif
      <div class="form-group">
        <button class="btn btn-primary" id="submit-thread" type="submit">Update</button>
      </div>
      </form>
    </div>
    <div class="col-lg-4">
      <h3>Posting Instructions</h3>
      <ul>
        <li>You must select a tag, and enter a title and body.</li>
        <li>Titles have a maximum length of 100 characters.</li>
        <li>To bold, italicize or add a link please select the text before clicking the button.</li>
        <li>To remove an existing image, place your cursor to the right of it and hit delete</li>
        <li>Add an image by clicking the <span style="height:20px"><svg style="height:25px;width:25px"><use xlink:href="#trumbowyg-insert-image"></use></svg></span> button in the toolbar
          <ul>
            <li>This will bring up a window to select your image</li>
            <li>Select or drag and drop one or more images</li>
            <li>After your image uploads the orage "Done" button will be active, click it</li>
            <li>Images do not appear in the edit window, they appear to the right of it (under it on mobile)</li>
            <li>To remove an image click it in the Post images list</li>
          </ul>
        </li>
      </ul>
      <h3>Post Images</h3>
      <p><small>Click image to remove from post</small></smal></p>
      <div class="row" id="post-images">

      </div>
    </div>
  </div>
  </container>
  @endsection


  @push('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.25.1/trumbowyg.min.js" crossorigin="anonymous">
  </script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.25.1/plugins/cleanpaste/trumbowyg.cleanpaste.min.js"
    crossorigin="anonymous"></script>
  <script src="https://upload-widget.cloudinary.com/global/all.js" type="text/javascript"></script>
  <script>
    $(document).ready(function(){ 
      $.noConflict()
    })
    $('#body').trumbowyg({
      btnsDef: {
        img: {
            fn: function() {
                myWidget.open()
            },
            ico: 'insertImage',
            class: 'upload_widget'
        }
      },
      btns: [['strong', 'em'],['justifyLeft', 'justifyCenter', 'justifyRight'], ['img', 'link']],
      autogrow: true,
      minimalLinks: true,
    });

    var generateSignature = function (callback, params_to_sign) {
      $.ajax({
        url: "/generate_signature",
        type: "GET",
        dataType: "text",
        data: {data: params_to_sign},
        complete: function () {console.log("complete")},
        success: function (signature, textStatus, xhr) {callback(signature);},
        error: function (xhr, status, error) {console.log(xhr, status, error);}
      });
    }
    let postImages = []
    let myWidget = cloudinary.createUploadWidget({
      cloudName: 'ckwraps', 
      clientAllowedFormats: ['jpg', 'jpeg', 'webp', 'png'],
      maxImageFileSize: 10000000,
      apiKey: '195169815959463',
      uploadSignature: generateSignature,
      uploadPreset: 'ckforum'}, (error, result) => { 
        if (!error && result && result.event === "success") { 
          postImages.push(result.info.secure_url)
          console.log(postImages)
          $('#post-images').html('')
          for( let x = 0; x < postImages.length; x++ ){
            let insert = '<div class="col-4"><img class="post-image" data-postid="'+ x + '" src="' + postImages[x] + '" style="max-width: 100%" /></div>'
            $('#post-images').append(insert)
          }
        }
      }
    )

    $(document).on('click', '.post-image', function() {
      console.log($(this).data('postid'))
      postImages.splice($(this).data('postid'), 1)
      $('#post-images').html('')
      for( let x = 0; x < postImages.length; x++ ){
        let insert = '<div class="col-4"><img class="post-image" data-postid="'+ x + '" src="' + postImages[x] + '" style="max-width: 100%" /></div>'
        $('#post-images').append(insert)
      }
    })

    $('#forum-form').submit(function(e) {
      e.preventDefault()
      if(postImages.length > 0) {
        console.log(postImages)
        for(let i = 0; i < postImages.length; i++) {
          $('#body').val( $('#body').val() + '<img class="post-image" data-postid="'+ i + '" src="' + postImages[i] + '" style="max-width: 100%" />')
        }
        $('#forum-form')[0].submit()
      } else {
        $('#forum-form')[0].submit()
      }
    })

  </script>
  @endpush