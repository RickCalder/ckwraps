@extends('layouts.app')

@section('extra-head')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.19.1/ui/trumbowyg.min.css"
  integrity="sha256-iS3knajmo8cvwnS0yrVDpNnCboUEwZMJ6mVBEW1VcSA=" crossorigin="anonymous" />
@endsection


@section('content')
<div class="container" style="margin-top: 100px">
  <div class="row">
    <div class="col-lg-12">
      <p class="forum-breadcrumbs">
        <a href="{{ route('forums') }}">{{ __('Back to Forums') }}</a>
      </p>
    </div>
    <div class="col-lg-12">
      <div class="d-flex align-items-center">
        <div style="align-self: flex-start">
        @if($thread->user->avatar)
        <div>
          <img src="{{ Storage::url('/avatars/'.$thread->user->avatar->image) }}" class="avatar avatar-md" />
        </div>
        @else
        <div class="avatar avatar-md avatar-initials d-flex align-items-center justify-content-center">
          <span style="line-height:0">
            {{substr($thread->user->name,0,1)}}
          </span>
        </div>
        @endif
      </div>
        <div>
          <h1 class="h2 thread-title ml-3">{{ $thread->title }}</h1>
          <p class="h5 ml-3">Posted by {{ $thread->user->name }} - {{ $thread->created_at->diffForHumans() }}
            @if(isset($thread->tag))
            in
            <a href="{{ route('forums'). '/tags/' . $thread->tag->tag_slug }}"
              class="badge badge-pill badge-success">{{ $thread->tag->tag_name }}</a>
            @endif </p>
          <div class="ml-3">
            @include('forums.forumcontrols')
          </div>
        </div>
      </div>
      <hr>
      <div class="thread-body">{!! $thread->body !!} </div>
    </div>
  </div>
  <div class="row mt-3">
    <div class="col-lg-12">
      <hr class="mt-2 mb-4">
    </div>
    <div class="col-lg-8">
      <h4>Reply</h4>
      @guest
      <div class="info">
        <p>You must <a href="{{ route('login') }}">{{ __('Login') }}</a> or <a
            href="{{ route('register') }}">{{ __('Register') }}</a> to reply to this thread!</p>
      </div>
      @else
      @csrf
      {{Form::open(['action' => ['ReplyController@store', 'thread_id' => $thread->id], 'id' => 'forum-form'])}}
      {{ Form::hidden('thread_id', $thread->id) }}
      {{ Form::hidden('slug', $thread->slug) }}
      <input type="hidden" name="quoted" id="quoted"/> 
      <div class="form-group">
        <label class="sr-only" for="body">Body</label>
        <textarea id="body" name="body">
          {{ old('body') }}
        </textarea>
      </div>
      @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div><br />
      @endif
      <div class="form-group">
        <button class="btn btn-primary" id="submit-thread" type="submit">Submit</button>
      </div>
      {{ Form::close() }}
      @endguest
    </div>
    <div class="col-lg-4">
      <h3>Images</h3>
      <li>Add an image by clicking the <span style="height:20px"><svg style="height:25px;width:25px"><use xlink:href="#trumbowyg-insert-image"></use></svg></span> button in the toolbar
        <ul>
          <li>This will bring up a window to select your image</li>
          <li>Select or drag and drop one or more images</li>
          <li>After your image uploads the orage "Done" button will be active, click it</li>
          <li>Images do not appear in the edit window, they appear to the right of it (under it on mobile)</li>
          <li>To remove an image click it in the Post images list</li>
        </ul>
      </li>
      <h3>Post Images</h3>
      <p><small>Click image to remove from post</small></smal></p>
      <div class="row" id="post-images">

      </div>
    </div>

  </div>
  <div class="row mt-3">
    <div class="col-lg-12">
      <h3>Replies</h3>
      @foreach( $thread->replies->reverse() as $reply )
      <div class="card mb-3">
        <div class="card-body reply-body">
          <div class="d-block d-md-flex align-items-center justify-content-md-between">
            <div  class="d-flex align-items-center">
        @if($reply->owner->avatar)
        <div style="align-self: flex-start">
          <img src="{{ Storage::url('/avatars/'.$reply->owner->avatar->image) }}" class="avatar avatar-xs" />
        </div>
        @else
        <div class="avatar avatar-xs avatar-initials d-flex align-items-center justify-content-center">
          <span style="line-height:0">
            {{substr($reply->owner->name,0,1)}}
          </span>
        </div>
        @endif
            <h6 class="d-flex ml-2 mb-0">{{ $reply->owner->name }} replied {{ $reply->created_at->diffForHumans()}}</h6>
            </div>
            @include('forums.replycontrols')
          </div>
          <hr>
          <div class="card-text">{!! $reply->body !!}</div>
          <div class="d-flex justify-content-end">
            <button class="reply btn btn-success btn-sm" data-commentid="{{$reply->id}}">Reply</button>
          </div>
          
        </div>
        @if($reply->quoted_id !== null)
        @if(isset($reply->quoted))
        <div class="video-quote">
          <p class="mb-1">
            Replying to: {{$reply->quoted->owner->name}} <span style="font-size:0.875rem">{{ $reply->quoted->created_at->diffForHumans() }}</span>
          </p>
          {{ html_entity_decode(strip_tags($reply->quoted->body)) }}
        </div>
        @else
        <div class="video-quote">
          The quoted reply was deleted.
        </div>
        @endif
      @endif
        @if($thread->announcement === 1)
        <div class="card-footer vote-footer">
          <div><strong>Votes: </strong><span id="count-{{$reply->id}}">{{ count($reply->favourites)}}</span></div>
          @if(Carbon\Carbon::now() < $thread->vote_expiry)
            @if($my_fave === null || $my_fave->reply_id !== $reply->id)
            <div><button class="btn btn-sm btn-success vote-button vote" data-count="{{count($reply->favourites)}}"
                data-votefor="{{$reply->id}}" data-thread_id="{{$thread->id}}">Vote for Me!</button></div>
            @else
            <div><button class="btn btn-sm btn-success vote-button remove" data-count="{{count($reply->favourites)}}"
                data-votefor="{{$reply->id}}" data-thread_id="{{$thread->id}}">Remove Vote</button></div>
            @endif
            <div><strong>Voting closes:
              </strong>{{ \Carbon\Carbon::parse($thread->vote_expiry)->isoFormat('MMMM Do YYYY') }}
            </div>
            @else
            <div>
              <p>Voting is now closed!</p>
            </div>
            @endif
        </div>
        @endif
      </div>
      @endforeach
    </div>
  </div>
</div>

@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.25.1/trumbowyg.min.js" crossorigin="anonymous">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.25.1/plugins/cleanpaste/trumbowyg.cleanpaste.min.js"
  crossorigin="anonymous"></script>
<script src="https://upload-widget.cloudinary.com/global/all.js" type="text/javascript"></script>
<script>
  $(document).ready(function(){ 
    $.noConflict()
  })
  $('#body').trumbowyg({
    btnsDef: {
      img: {
          fn: function() {
              myWidget.open()
          },
          ico: 'insertImage',
          class: 'upload_widget'
      }
    },
    btns: [['strong', 'em'],['justifyLeft', 'justifyCenter', 'justifyRight'], ['img', 'link']],
    autogrow: true,
    minimalLinks: true,
  });

  var generateSignature = function (callback, params_to_sign) {
    $.ajax({
      url: "/generate_signature",
      type: "GET",
      dataType: "text",
      data: {data: params_to_sign},
      complete: function () {console.log("complete")},
      success: function (signature, textStatus, xhr) {callback(signature);},
      error: function (xhr, status, error) {console.log(xhr, status, error);}
    });
  }
  let postImages = []
  let myWidget = cloudinary.createUploadWidget({
    cloudName: 'ckwraps', 
    clientAllowedFormats: ['jpg', 'jpeg', 'webp', 'png'],
    maxImageFileSize: 10000000,
    apiKey: '195169815959463',
    uploadSignature: generateSignature,
    uploadPreset: 'ckforum'}, (error, result) => { 
      if (!error && result && result.event === "success") { 
        postImages.push(result.info.secure_url)
        console.log(postImages)
        $('#post-images').html('')
        for( let x = 0; x < postImages.length; x++ ){
          let insert = '<div class="col-4"><img class="post-image" data-postid="'+ x + '" src="' + postImages[x] + '" style="max-width: 100%" /></div>'
          $('#post-images').append(insert)
        }
      }
    }
  )

  $(document).on('click', '.post-image', function() {
    console.log($(this).data('postid'))
    postImages.splice($(this).data('postid'), 1)
    $('#post-images').html('')
    for( let x = 0; x < postImages.length; x++ ){
      let insert = '<div class="col-4"><img class="post-image" data-postid="'+ x + '" src="' + postImages[x] + '" style="max-width: 100%" /></div>'
      $('#post-images').append(insert)
    }
  })

  $('#forum-form').submit(function(e) {
    e.preventDefault()
    if(postImages.length > 0) {
      console.log(postImages)
      for(let i = 0; i < postImages.length; i++) {
        $('#body').val( $('#body').val() + '<img class="post-image" data-postid="'+ i + '" src="' + postImages[i] + '" style="max-width: 100%" />')
      }
      $('#forum-form')[0].submit()
    } else {
      $('#forum-form')[0].submit()
    }
  })

</script>

<script>
  $('#delete-thread').on('click', function(e) {
      e.preventDefault()
      const result = confirm('Are you sure you want to delete this thread?')

      if(result) {
        $('#delete-form').submit()
      } else {
        console.log('canceled')
      }
    })
    $('#delete-reply').on('click', function(e) {
      e.preventDefault()
      const result = confirm('Are you sure you want to delete this reply?')

      if(result) {
        $('#delete-reply-form').submit()
      } else {
        console.log('canceled')
      }
    })

    $('.vote-button').on('click', function(e) {
      e.preventDefault()
      console.log($(this).data('thread_id'))

      if($(this).hasClass('vote')) {
        $.ajax({
          url: '/vote',
          method: 'POST',
          data: {
            'reply_id': $(this).data('votefor'),
            'thread_id': $(this).data('thread_id'),
            "_token": "{{ csrf_token() }}",
            'requestType' : 'add'
            },
          dataType: 'json',
          success: function(result) {
            location.reload()
          }

        })
      } else {
        $.ajax({
          url: '/vote',
          method: 'POST',
          data: {
            'reply_id': $(this).data('votefor'),
            'thread_id': $(this).data('thread_id'),
            "_token": "{{ csrf_token() }}",
            'requestType' : 'remove'
            },
          dataType: 'json',
          success: function(result) {
            location.reload()
          }

        })
        
      }
    })
</script>

  <script>
    $(document).on('click', '.reply', function() {
      $('.reply').html('Reply')
      $('#body').focus()
      $(this).html('Replying To')
      $('#quoted').val($(this).attr('data-commentid'))
    })
  </script>
@endpush

