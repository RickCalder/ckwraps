<nav class="navbar navbar-expand-xl navbar-dark bg-dark fixed-top">
  <a class="navbar-brand" href="{{ url('/') }}">
    <img src={{ url('/img/ckwraps-logo.png')}} alt="CKWraps logo home link" />
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <!-- Left Side Of Navbar -->
    <ul class="navbar-nav mr-auto">

    </ul>

    <!-- Right Side Of Navbar -->
    <ul class="navbar-nav ml-auto">
      <!-- Authentication Links -->

      @if ( isset($giveaway))
      <li class="nav-item">
        <a class="nav-link btn btn-success" href="/giveaway">{{ __($giveaway->button_text) }}</a>
      </li>
      @endif
      <li class="nav-item">
        <a class="nav-link {{ Request::is('/') ? 'active' : '' }}" href="{{ route('home') }}">{{ __('Home') }}</a>
      </li>
      <li class="nav-item">
        <a class="nav-link {{ Request::is('forums*') ? 'active' : '' }}"
          href="{{ route('forums') }}">{{ __('Forums') }}</a>
      </li>
      <li class="nav-item">
        <a class="nav-link {{ Request::is('videos*') ? 'active' : '' }}"
          href="{{ route('videos') }}">{{ __('Videos') }}</a>
      </li>
      
      <li class="nav-item">
        <a class="nav-link"
          href="https://www.vinylwraptraining.com/" target="_blank">{{ __('Training') }}</a>
      </li>
      <li class="nav-item">
        <a class="nav-link {{ Request::is('advertise') ? 'active' : '' }}"
            href="{{ route('advertise') }}">{{ __('Advertise') }}</a>
      </li>
      <li class="nav-item">
        <a class="nav-link {{ Request::is('questions-answers') ? 'active' : '' }}"
          href="{{ route('faq') }}">{{ __('Q & A') }}</a>
      </li>
      <li class="nav-item">
        <a class="nav-link {{ Request::is('shop') ? 'active' : '' }}" href="{{ route('shop') }}">{{ __('Shop') }}</a>
      </li>
      <li class="nav-item dropdown">
        <a id="navbarDropdown"
          class="nav-link dropdown-toggle {{ Request::is('resources') ? 'active' : '' }} {{ Request::is('locations') ? 'active' : '' }}"
          href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
          Resources <span class="caret"></span>
        </a>

        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
          <a class="dropdown-item {{ Request::is('resources') ? 'active' : '' }}"
            href="{{ route('resources') }}">{{ __('Sponsors') }}</a>
            
            <a class="dropdown-item {{ Request::is('testimonials') ? 'active' : '' }}"
            href="{{ route('testimonials') }}">{{ __('Premium Member Testimonials') }}</a>
            @if($downloads->first())
            <a class="dropdown-item {{ Request::is('important-documents') ? 'active' : '' }}"
            href="{{ route('downloads') }}">{{ __('Important Documents') }}</a>
            @endif
                  
            @if(isset($liveEvent) && $liveEvent->count() > 0)
              <a class="dropdown-item {{ Request::is('live-events') ? 'active' : '' }}"
                href="{{ route('live-events') }}"><i class="fa fa-video-camera" style="color:#04AA6D"></i> {{ __('Live Now') }}</a>
            @else
              <a class="dropdown-item {{ Request::is('live-events') ? 'active' : '' }}"
                href="{{ route('live-events') }}">{{ __('Live') }}</a>
            @endif

        </div>
      </li>      
      <li class="nav-item">
        <a class="nav-link {{ Request::is('contact') ? 'active' : '' }}"
          href="{{ route('contact') }}">{{ __('Contact') }}</a>
      </li>
      @guest
      <li class="nav-item">
        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
      </li>
      @if (Route::has('register'))
      <li class="nav-item">
        <a class="nav-link btn btn-primary" href="{{ route('register') }}">{{ __('Join Now') }}</a>
      </li>
      @endif
      @else
      <li class="nav-item dropdown">
        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown"
          aria-haspopup="true" aria-expanded="false" v-pre>
          {{ Auth::user()->name }} <span class="caret"></span>
        </a>

        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
          @if( @auth()->user()->role_id === 1)
          <a class="dropdown-item" href="/auth">
            {{ __('Admin') }}
          </a>
          @endif
          <a class="dropdown-item" href="{{ route('account') }}">
            {{ __('My Account') }}
          </a>

          <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                          document.getElementById('logout-form').submit();">
            {{ __('Logout') }}
          </a>

          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
          </form>
        </div>
      </li>
      @endguest
      @if (auth()->user() && auth()->user()->role_id === 3)
      <li class="nav-item">
        <a class="nav-link btn btn-primary" href="/plans?upgrade=1">{{ __('Upgrade Now!') }}</a>
      </li>
      @endif
    </ul>
  </div>
</nav>