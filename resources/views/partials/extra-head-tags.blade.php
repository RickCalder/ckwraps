

@php 
function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

$hash = generateRandomString();
@endphp

<title>{{$pageData->title}}</title>
<meta name="description" content="{{$pageData->description}}">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="robots" content="follow"/>

<!-- Open Graph Tags -->
<meta property="og:url" content="{{url()->current()}}" />
<meta property="og:type" content="website" />
<meta property="og:title" content="{{$pageData->title}}" />
<meta property="og:description" content="{{$pageData->description}}" />
<meta property="og:image" content="{{url('/') . Storage::url($pageData->image)}}" />

<!-- Twitter Cards -->
<meta property="twitter:title" content="{{$pageData->title}}" />
<meta name="twitter:card" content="summary_large_image" />
<meta property="twitter:image" content="{{url('/') . Storage::url($pageData->image)}}?{{$hash}}" />
<meta property="twitter:description" content="{{$pageData->description}}" />
