<footer>
  <div class="footer-container container">
    <div class="row">
      <div class="col-md-4">
        <h3>About Us</h3>
      <p><small>Our website will arm you with the right tools to become a seasoned pro in custom vehicle wrapping. We help you gain self-sufficiency and knowledge through: personalized support, online video training in 4K resolution, exclusive resources, location boards, and communication boards for troubleshooting. You will also be able to showcase your business through our platform, which can help you gain exposure and clientele. Website members will have exclusive access to all of our valuable content, which is consistently being updated.</small></p>
      </div>
      <div class="col-md-4">
        <h3>CKWraps Social Media</h3>
        <div class="mt-4 d-flex justify-content-around">
            <a href="http://www.youtube.com/c/CKWraps" target="_blank" rel="nofollow noopener">
              <img src="/img/youtube.png" alt="Youtube Icon">
              <span class="sr-only">Link to CKWraps Youtube account</span>
            </a>
            <a href="https://www.facebook.com/CKwrap/" target="_blank" rel="nofollow noopener">
              <img src="/img/facebook.png" alt="Facebook Icon">
              <span class="sr-only">Link to CKWraps Facebook account</span>
            </a>
            <a href="https://www.instagram.com/ckwraps/" target="_blank" rel="nofollow noopener">
              <img src="/img/instagram.png" alt="Instagram Icon">
              <span class="sr-only">Link to Instagram Youtube account</span>
            </a>
        </div>
        <h3 class="mt-4">Subscribe to our Newsletter</h3>
        {{ Form::open(['route' => 'mcsubscribe', 'class' =>['mb-4']]) }}
        <div class="form-group mb-4">
          <label for="mc_email" class="sr-only">Email address</label>
          <input type="email" name="mc_email" class="form-control" id="mc_email" aria-describedby="emailHelp" placeholder="Enter email">
          <small id="emailHelp" class="form-text text-muted sr-only">We'll never share your email with anyone
            else.</small>
          <div class="subscribe-success alert alert-success small">
            Thank you! You should receive a verification email soon.
          </div>
        </div>
        <button type="submit" id="mc-subscribe" class="btn btn-primary">Submit</button>
        {{ Form::close() }}
      </div>
      <div class="col-md-4">
        <h3>Site Links</h3>
        <ul>
          <li>
            <a class="{{ Request::is('/') ? 'active' : '' }}" href="/">{{ __('Home') }}</a>
          </li>
          
          <li class="nav-item">
            <a class="{{ Request::is('forums*') ? 'active' : '' }}" href="{{ route('forums') }}">{{ __('Forums') }}</a>
          </li>
          @guest
          <li>
            <a class="{{ Request::is('register') ? 'active' : '' }}"
              href="{{ route('register') }}">{{ __('Join Now') }}</a>
          </li>
          @endguest
          <li>
            <a class="{{ Request::is('questions-answers') ? 'active' : '' }}"
              href="{{ route('faq') }}">{{ __('Q & A') }}</a>
          </li>
          <li>
            <a class="{{ Request::is('shop') ? 'active' : '' }}"
              href="{{ route('shop') }}">{{ __('Shop') }}</a>
          </li>
          <li>
            <a class="{{ Request::is('resources') ? 'active' : '' }}"
              href="{{ route('resources') }}">{{ __('Resources') }}</a>
          </li>
          <li>
            <a class="{{ Request::is('advertise') ? 'active' : '' }}"
            href="{{ route('advertise') }}">{{ __('Advertise With Us') }}</a>
          </li>
          <li>
            <a class="{{ Request::is('training') ? 'active' : '' }}"
              href="{{ route('training') }}">{{ __('Colour Change Certification') }}</a>
          </li>
          <li>
            <a class="{{ Request::is('ppf-training') ? 'active' : '' }}"
              href="{{ route('ppf-training') }}">{{ __('PPF Certification Workshop') }}</a>
          </li>
          <li>
            <a class="{{ Request::is('terms-of-service') ? 'active' : '' }}"
              href="{{ route('tos') }}">{{ __('Terms of Service') }}</a>
          </li>
          <li>
            <a class="{{ Request::is('privacy') ? 'active' : '' }}"
              href="{{ route('privacy') }}">{{ __('Privacy Policy') }}</a>
          </li>
        </ul>
      </div>
    </div>
    <hr>
    <div class="row mt-3">
      <div class="col text-center">
        <p class="copyright">&copy; {{ now()->year }} CK Wraps Inc. All rights reserved</p>
      </div>
    </div>
  </div>
</footer>

@push('scripts')
<script>
  jQuery(document).ready(function(){
    jQuery('#mc-subscribe').click(function(e){
        e.preventDefault();
        $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
          }
      });
        jQuery.ajax({
          url: "{{ url('/mcsubscribe') }}",
          method: 'post',
          data: {
            _token: "{{ csrf_token() }}",
              email: jQuery('#mc_email').val(),
          },
          success: function(result){
            console.log(result);
            jQuery('.subscribe-success').slideDown()
          }});
        });
    });
</script>
@endpush