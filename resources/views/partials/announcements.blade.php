
      <div class="card mb-3 {{ $announcement->tag_slug }} @if($announcement->announcement>=1)card-announcement @endif">
          <div class="card-body">
            <div class="d-flex">
              <div>
                @if($announcement->user->avatar)
                <div>
                  <img src="{{ Storage::url('/avatars/'.$announcement->user->avatar->image) }}" class="avatar avatar-sm" />
                </div>
                @else
                <div class="avatar avatar-sm avatar-initials d-flex align-items-center justify-content-center">
                  <span style="line-height:0">
                    {{substr($announcement->user->name,0,1)}}
                  </span>
                </div>
                @endif
              </div>
              <div class="ml-2 w-100">
            @if($allowed)
            <a class="forum-announcement-link" href="{{ route('thread', $announcement->slug) }}">
              @endif
              <div class="d-flex justify-content-between">
                <h5 class="card-title d-flex mb-1"><strong>{{ $announcement->title }}@if($announcement->announcement>=1) - Annoucement @endif</strong></h5>
                <p class="d-flex h6"><i class="fa fa-comment" aria-hidden="true" style="color:#bbb">&nbsp;</i>
                  {{ count($announcement->replies) }}</p>
              </div>
              <div class="card-text forum-excerpt">
                @if($allowed)
                {{ str_limit(strip_tags($announcement->body), $limit = 100, $end = '...') }}
                @else
                <p>To view the contents of this announcement you must be a premium member, either <a
                    href="{{route('login')}}">login</a> or <a href="{{route('register')}}">Join Now</a></p>
                @endif
              </div>
              @if($allowed)
            </a>
            @endif
            @if($allowed)
            @if(count($announcement->replies) === 0)
              <p class="h6 mt-3">
                <a href="/forums/user/{{$announcement->user->id}}">{{ $announcement->user->name }}</a>&nbsp; posted
                {{ $announcement->created_at->diffForHumans() }} in &nbsp;<a
                href="/forums/tags/{{$announcement->tag->tag_slug}}">{{$announcement->tag->tag_name}}</a></p>
              @else
              <p class="h6 mt-3">
                <a href="/forums/user/{{$announcement->user->id}}">{{ $announcement->user->name }}</a>&nbsp; posted
                {{ $announcement->created_at->diffForHumans() }} in &nbsp;<a
                href="/forums/tags/{{$announcement->tag->tag_slug}}">{{$announcement->tag->tag_name}}</a></p>
              <p class="h6">
                @php 
                $reply_user = count($announcement->replies) - 1;
                @endphp
                <a href="/forums/user/{{$announcement->replies[0]->id}}">{{ $announcement->replies[$reply_user]->owner->name }}</a>&nbsp; replied
                {{ $announcement->updated_at->diffForHumans() }}</p>
              @endif
            @else 
            @if(count($announcement->replies) === 0)
            <p class="d-flex h6 mt-3">
              <a href="#">{{ $announcement->user->name }}</a>&nbsp; posted
              {{ $announcement->created_at->diffForHumans() }} in &nbsp;<a
              href="#">{{$announcement->tag->tag_name}}</a></p>
            @else
            <p class="d-flex h6 mt-3">
              <a href="#">{{ $announcement->user->name }}</a>&nbsp; posted
              {{ $announcement->created_at->diffForHumans() }} in &nbsp;<a
              href="#">{{$announcement->tag->tag_name}}</a></p>
            <p class="d-flex h6">
              @php 
              $reply_user = count($announcement->replies) - 1;
              @endphp
              <a href="#">{{ $announcement->replies[$reply_user]->owner->name }}</a>&nbsp; replied
              {{ $announcement->updated_at->diffForHumans() }}</p>
            @endif
            @endif
              </div>
            </div>
          </div>
        </div>