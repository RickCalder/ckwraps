
<div class="card mt-3">
    <div class="card-header">
      {{ $reply->owner->name }} replied {{ $reply->created_at->diffForHumans() }}
    </div>
    <div class="card-body">
          <article>
          <div>{{ $reply->body }}</div>
          </article>
    </div>
</div>