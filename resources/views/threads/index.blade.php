@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Forum Threads</div>

                <div class="card-body">
                    @foreach($threads as $thread)
                      <article class="mt-3">
                      <h4>
                        <a href="{{ $thread->path() }}">{{ $thread->title }}</a> by {{ $thread->owner->name }}
                      </h4>
                      <div>{{ $thread->body }}</div>
                      </article>
                      <hr>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
