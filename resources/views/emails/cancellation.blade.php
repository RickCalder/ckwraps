<h2>Hello,</h2>
<p>{{ $name }} has cancelled their subscription</p>
<p>Here are the details:</p>
<p>Name: {{ $name }}</p>
<p>Email: {{ $email }}</p>
<p>Reason: {{ $user_message }}</p>
<p>Thank You</p>