<h2>Hello,</h2>
<p>{{ $name }} has created a new subscription</p>
<p>Here are the details:</p>
<p>Name: {{ $name }}</p>
<p>Email: {{ $email }}</p>
<p>Found us: {{ $user_message }}</p>
<p>Thank You</p>