<h2>Hello,</h2>
<p>You received a new video comment from : {{ $name }}</p>
<p>Here are the details:</p>
<p>Video: {{ $video_link }}</p>
<p>Comment: {{ $comment }}</p>