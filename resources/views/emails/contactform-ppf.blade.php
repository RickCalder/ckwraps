<h2>Hello,</h2>
<p>You received an email from : {{ $name }}</p>
<p>Subject: {{ $subject }}</p>
<p>Here are the details:</p>
<p>Name: {{ $name }}</p>
<p>Business Name: {{ $business }}</p>
<p>Email: {{ $email }}</p>
<p>Phone Number: {{ $phone }}</p>
<p>Number of applicants: {{$applicants}} </p>
<p>Profile: {{ $user_message }}</p>
<p>Address: {{$address}}</p>
<p>Heard about us: {{$hear}} </p>
<p>Thank You</p>