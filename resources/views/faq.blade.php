@extends('layouts.app')

@section('top-content')

<div class="fullscreen">
  <div class="slide-mask">
      <div class="slide-content">
        <div class="container">
          <h1 class="slide-content-title">Questions &amp; Answers</h1>
          <div class="slide-content-body">
              <p>Everything you want to know about Vinyl Wrapping</p>
              @include('partials.join_button')
          </div>
        </div>
      </div>
      <div class="slides-control">
         <div class="slides-control-down">
           Learn More
         </div>
       </div>
    </div>
  <img 
    class="cover"
    srcset="/img/camarao-576.jpg 576w, /img/camaro-1024.jpg 1024w, /img/camaro-1920.jpg 1920w"
    sizes="(max-width: 576px) 576px,
    (max-width: 1024px) 1024px,
    (max-width: 1920px) 1920px"
  src="/img/camaro.jpg" alt="Blue Chrome wrapped Audi">
</div>
@endsection

@section('content')
<div class="container mt-5 scroll-target">
  <div class="row mb-5">
    <div class="col-lg-8 offset-lg-2">
      @foreach ($faqs as $faq)
        <h3>{{ $faq->question }}</h3>
        {!! $faq->answer !!}
        <hr>
      @endforeach
  </div>
</div>
@endsection

@push('scripts')
<script>
  window.onload = () => {
    document.querySelector('.slides-control').addEventListener('click', () => {
      const top = document.querySelector('.scroll-target').offsetTop
      const offset = document.querySelector('.navbar').offsetHeight + 50
      window.scrollTo(0, top - offset)
    });
  }
</script>
@endpush
