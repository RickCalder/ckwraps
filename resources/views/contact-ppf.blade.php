@extends('layouts.app')
@section('extra-head')
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
@endsection


@section('content')
<div class="container" style="margin-top: 100px">
  <div class="row mb-5">
    <div class="col-lg-12">
      <h1>PPF (Paint Protection Film) Registration</h1>
    </div>
  </div>
  
  <div class="row text-center">
    <div class="col-lg-12">
    <p>CK Wraps is happy to serve you from our location in Ottawa, Ontario, Canada</p>
    </div>
  </div>
  @php
  $user = auth()->user();
  $email = '';
  $name = '';
  $message = '';
  if( $user !== null ) {
  $email = $user->email;
  $name = $user->name;
  }

  if( old('name') ) {
  $name = old('name');
  }

  if( old('email') ) {
  $email = old('email');
  }

  if( old('subject') ) {
  $subject = old('subject');
  }

  if( old('message') ) {
  $message = old('message');
  }
  @endphp
  <div class="row">
    <div class="col-lg-12">
      <section class="mb-4">
        <div class="row">
          <div class="col-lg-8 mb-lg-0 mb-5 offset-lg-2">
            @if (session('status'))
            <div class="alert alert-success">
              {{ session('status') }}
            </div>
            @endif
            @csrf
            {{Form::open(['action' => 'ContactController@mail_ppf', 'method'=>"POST", 'id' => 'contact-form-ppf'])}}
            <div class="row mb-3">
              <div class="col-lg-12">
                <div class="lg-form mb-0">
                  <label for="name" class="">Full Name *</label>
                  <input type="text" id="name" name="name" class="form-control" value="{{$name}}" required>
                </div>
              </div>
            </div>
            <div class="row mb-3">
              <div class="col-lg-12">
                <div class="lg-form mb-0">
                  <label for="business" class="">Business Name</label>
                  <p class="small">Required for business Registrations</p>
                  <input type="text" id="business" name="business" class="form-control">
                </div>
              </div>
            </div>
            <div class="row mb-3">
              <div class="col-lg-12">
                <div class="lg-form mb-0">
                  <label for="email" class="">Email *</label>
                  <input type="email" id="email" name="email" class="form-control" value="{{$email}}" required>
                </div>
              </div>
            </div>
            <div class="row mb-3">
              <div class="col-lg-12">
                <div class="lg-form mb-0">
                  <label for="phone" class="">Phone Number *<small>(please include country and area
                      codes)</small></label>
                  <input type="text" id="phone" name="phone" class="form-control" required>
                </div>
              </div>
            </div>
            <div class="row mb-3">
              <div class="col-lg-12">
                <div class="lg-form mb-0">
                  <label for="subject" class="">Website or Social Media</label>
                  <input type="text" id="website" name="website" class="form-control">
                </div>
              </div>
            </div>
            <div class="row mb-3">
              <div class="col-lg-12">
                <div class="lg-form mb-0">
                  <label for="applicants" class="">Number of Applicants</label>
                  <p class="small">Required for business Registrations</p>
                  <select type="text" id="applicants" name="applicants" class="form-control">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4+</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="row mb-3">
              <div class="col-lg-12">
                <div class="lg-form">
                  <label for="address">Address</label>
                  <p class="small">Please include full mailing address, including postal/zip code and country</p>
                  <textarea type="text" id="address" name="address" rows="6" class="form-control lg-textarea"
                    required>{{$message}}</textarea>
                </div>
              </div>
            </div>
            <div class="row mb-3">
              <div class="col-lg-12">
                <div class="lg-form">
                  <label for="profile">Profile</label>
                  <p class="small">Tell us what you know, what you want to focus on the most and what your expectations are regarding this certification program.</p>
                  <textarea type="text" id="profile" name="profile" rows="6" class="form-control lg-textarea"
                    required>{{$message}}</textarea>
                </div>
              </div>
            </div>
            <div class="row mb-3">
              <div class="col-lg-12">
                <div class="lg-form mb-0">
                  <label for="hear" class="">How did you hear about CK Wraps</label>
                  <select type="text" id="hear" name="hear" class="form-control">
                    <option value="Facebook">Facebook</option>
                    <option value="Instagram">Instagram</option>
                    <option value="YouTube">YouTube</option>
                    <option value="Other">Other</option>
                  </select>
                </div>
              </div>
            </div>

            <div class="row mb-3">
              <div class="col-lg-12">
                <div class="lg-form">
                  <div class="g-recaptcha" data-sitekey="6LevhcQUAAAAAOIg4xM5wBNzUgY6VV4oCMSeUXov">
                  </div>
                  <span role="alert" class="invalid-feedback d-block">
                    <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                  </span>
                </div>
              </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger mt-3">
              <ul style="margin:0">
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div><br />
            @endif
            <div class="text-center text-lg-left">
              <button type="submit" id="submit-form" class="btn btn-primary btn-lg">Send</button>
            </div>
            </form>
          </div>
        </div>
      </section>
      

    </div>
  </div>
</div>
@endsection

@push('scripts')
<script>
  $('#submit-form').on('click', (e) => {
      $('#submit-form').attr('disabled', true)
      var $myForm = $('#contact-form-ppf')
      if (!$myForm[0].checkValidity()) {
        // If the form is invalid, submit it. The form won't actually submit;
        // this will just cause the browser to display the native HTML5 error messages.
        $('#submit-form').attr('disabled', false)
        $myForm.find(':submit').click()
      } else {
        $('#contact-form-ppf').submit()
      }
    })
</script>
@endpush