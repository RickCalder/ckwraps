@extends('layouts.app')

@section('content')
<div class="container" style="margin-top:100px">
  <div class="row justify-content-center mb-4">
    <div class="col-lg-12">
      <h1 class="h2">Account Details</h1>
    </div>
  </div>
  <div class="row justify-content-center">
    @include('auth.account-sidebar')
    <div class="col-lg-8">
      @if (session('status'))
      <div class="alert alert-success">
        {{ session('status') }}
      </div>
      @endif
      <h2>Details</h2>
      {{Form::open(['action' => ['UserController@update'], 'method' => 'PUT'])}}
      @csrf
      <div class="form-group">
        <label for="name">Name</label>
        <input type="text" class="form-control" name="name" id="name" placeholder="Enter Name"
          value="@if(old('name')){{old('name')}}@else{{ $user->name }}@endif" required>
      </div>
      <div class="form-group">
        <label for="email">Email Address</label>
        <input type="email" class="form-control" name="email" id="email" placeholder="Enter Email"
          value="{{ $user->email }}" required>
      </div>
      <div class="form-group">
        @php 
          if($user->notifications == 1){
            $checked = 'checked="checked"';
          } else {
            $checked = '';
          }
        @endphp
        
        <label for="notifications"><input type="checkbox" name="notifications" id="notifications" {{$checked}}"/> Receive Video Comment Notifications</label>
      </div>
      <button type="submit" class="btn btn-primary">Update</button>
      </form>
      @if ($errors->any())
      <div class="alert alert-danger mt-4">
        <ul style="margin:0">
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div><br />
      @endif

    </div>
  </div>
  
  <div class="row justify-content-center">
    <div class="col-lg-4 mt-3"></div>
    <div class="col-lg-8 mt-3">
      <h2>Avatar</h2>
      @if($user->avatar)
        <img src="{{ Storage::url('/avatars/'.$user->avatar->image) }}" class="avatar avatar-md" />
      @else
        <div class="avatar avatar-md avatar-initials d-flex align-items-center justify-content-center">
          <span>
            {{substr($user->name,0,1)}}
          </span>
        </div>
      @endif
      <form action="{{ route('avatar') }}" method="POST" enctype="multipart/form-data">
        @csrf
      <input class="mt-3" type="file" name="avatar"/>
      <button class="btn btn-primary" type="submit">Save Avatar</button>
      </form>
      <p class="mt-3" style="font-size: 0.85rem">Note: Avatar images must be smaller than 1MB in file size, and preferably close to square in shape. Rectangle images will display the centre of the image only.</p>
    </div>
  </div>

</div>
@endsection