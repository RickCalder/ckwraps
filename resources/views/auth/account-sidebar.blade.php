<div class="col-lg-4">
  <ul>
    <li>
      <a href={{ route('account')}}>Account Details</a>
    </li>
    <li>
      <a href={{ route('subscription')}}>Subscription</a>
    </li>
    @if(\Request::get('userAllowed')) 
    <li>
      <a href={{ route('user_review')}}>Review CK Wraps</a>
    </li>
    @endif
  </ul>
</div>