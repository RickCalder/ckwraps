@extends('layouts.app')
@section('extra-head')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.19.1/ui/trumbowyg.min.css"
  integrity="sha256-iS3knajmo8cvwnS0yrVDpNnCboUEwZMJ6mVBEW1VcSA=" crossorigin="anonymous" />
@endsection

@section('content')
<div class="container" style="margin-top:100px">
  <div class="row justify-content-center mb-4">
    <div class="col-lg-12">
      <h1 class="h2">Subscription Details</h1>
    </div>
  </div>
  <div class="row justify-content-center">
    @include('auth.account-sidebar')
    <div class="col-lg-8">
      @if (session('status'))
      <div class="alert alert-success">
        {{ session('status') }}
      </div>
      @endif
      <p>CKWraps allows its premium members to list their businesses on our location board. You may add or edit your listing below. Please understand that your listing must meet our <a href="/terms-of-service">terms of service</a> standards. CKWraps reserves the right to remove any listing at our discretion.</p>
      @if( $location !== null && $location->published === 0)
      <p class="alert alert-danger">Your location has been unpublished. Please contact <a href="mailto:nav@ckrwraps.com">support</a> to find out why and how this can be rectified!</p>
      @endif

      @if( $location !== null)
      {{Form::open(['action' => ['UserController@update_location'], 'method' => 'PUT'])}}
      @else 
      {{Form::open(['action' => ['UserController@create_location'], 'method' => 'POST'])}}
      @endif
      @csrf
      <div class="form-group">
        <label for="name">Business Name</label>
        <input type="text" class="form-control" name="name" id="name"
          value="@if(old('name')){{old('name')}}@else @if($location !== null){{ $location->name }}@endif @endif" required>
      </div>
      <div class="form-group">
        <label for="email">Email Address</label>
        <input type="email" class="form-control" name="email" id="email"
          value="@if(old('email')){{old('email')}}@else @if($location !== null){{ $location->email }}@endif @endif" required>
      </div>
      <div class="form-group">
        <label for="website">Website <small><strong>(please include http or https ex: https://ckwraps.com, if this is missing your website will NOT display.)</strong></small></label>
        <input type="text" class="form-control" name="website" id="website"
          value="@if(old('website')){{old('website')}}@else @if($location !== null){{ $location->website }}@endif @endif" required>
      </div>
      <div class="form-group">
        <label for="phone">Phone Number <small>(Format: 123-456-7890)</small></label>
        <input type="text" class="form-control" name="phone" id="phone"
          value="@if(old('phone')){{old('phone')}}@else @if($location !== null){{ $location->phone }}@endif @endif" required>
      </div>
      <div class="form-group">
        <label for="address">Address <small>(Format: street address, city, province/state, postal/zip code, country)</small></label>
        <textarea class="form-control" name="address" id="address"
          required>@if(old('address')){{old('address')}}@else @if($location !== null){{ $location->address }}@endif @endif
        </textarea>
      </div>
      <div class="form-group">
        <label for="services">Description of Services</label>
        <textarea class="form-control" name="services" id="services"
           required>@if(old('services')){{old('services')}}@else @if($location !== null){{ $location->services }}@endif @endif
          </textarea>
      </div>
      @if( $location !== null )
      <button type="submit" class="btn btn-primary">Update</button>
      @else 
      <button type="submit" class="btn btn-primary">Create</button>
      @endif
      </form>


      @if ($errors->any())
      <div class="alert alert-danger mt-4">
        <ul style="margin:0">
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div><br />
      @endif

    </div>
  </div>
</div>
@endsection



@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.19.1/trumbowyg.min.js"
  integrity="sha256-1ifXbvyVBZsVmsqwqcoow46rXHi4976VpOWpaMVu2qM=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.19.1/plugins/cleanpaste/trumbowyg.cleanpaste.min.js" integrity="sha256-GGXtZ0tz4DfEMvShclGiegXJZt9r49+KqwWUvZ6+nlY=" crossorigin="anonymous"></script>
<script>
  $('#services').trumbowyg({
  btns: [['strong', 'em', 'link'],['unorderedList', 'orderedList'],['justifyLeft', 'justifyCenter', 'justifyRight']],
  autogrow: true,
  minimalLinks: true
});
</script>
@endpush