@extends('layouts.app')
@section('extra-head')
<style>
    .terms-error {
        display: none;
    }
</style>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
@endsection

@section('content')
<div class="container" style="margin-top:100px">
    <div class="row justify-content-center mt-4">
        <div class="col-lg-12">
            <div class="card-deck plans">
                @foreach($plans as $plan)
                @if( (app('request')->input('upgrade') === '1' && $plan->slug != 'free') ||
                app('request')->input('upgrade') === null)
                <div class="card mb-4">
                    <div class="card-header text-center">
                        <h2 class="h3">CK Wraps {{ $plan->name }} Subscription</h2>
                    </div>
                    <div class="card-body">
                        <div class="card-text">
                            <div class="text-center">
                                @if($plan->slug === "free")
                                <h3>Free</h3>
                                @else
                                <h3>${{ number_format($plan->cost, 2) }}/{{ $plan->recurring }}*</h3>
                                @endif
                            </div>
                            {!! $plan->description !!}
                        </div>
                    </div>
                </div>

                @endif
                <div class="w-100 d-block d-xl-none">
                    <!-- wrap every 3 on md-->
                </div>
                @endforeach
            </div>
        </div>
        <div class="text-center">
            <h2 class="my-3">Not sure yet? See what some of our current members have to say about us. <a
                    href="/testimonials">View Testimonials</a></h2>
                        
            <div class="alert alert-info offset-md-2 col-md-8">
            <p class="mb-0">Please note, any coupons or promotions are only available via Credit Card payment. They cannot be applied to a PayPal payment.</p>
            </div>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Join Now') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}" id="register-form">
                        @csrf

                        <div class="form-group row">
                            <label for="plan"
                            class="col-md-4 col-form-label text-md-right">{{ __('Choose a Plan') }}</label>
                            <div class="col-md-6">
                                <select class="form-control" id="plan" name="plan" autofocus>
                                    <option value="">Choose a Plan</option>
                                    <option value="monthly">Monthly - $8.99 / month</option>
                                    <option value="yearly">Yearly - $96 / year</option>
                                </select>

                                @error('plan')
                                <span class="invalid-feedback d-block" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name"
                                class="col-md-4 col-form-label text-md-right">{{ __('First Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                                    name="name" value="{{ old('name') }}" required autocomplete="name" >

                                @error('name')
                                <span class="invalid-feedback d-block" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="lastname"
                                class="col-md-4 col-form-label text-md-right">{{ __('Last Name') }}</label>

                            <div class="col-md-6">
                                <input id="lastname" type="text"
                                    class="form-control @error('name') is-invalid @enderror" name="lastname"
                                    value="{{ old('lastname') }}" autocomplete="lastname">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email"
                                class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                    name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="found"
                                class="col-md-4 col-form-label text-md-right">{{ __('How did you find us') }}</label>

                            <div class="col-md-6">
                                <select id="found" type="found" class="form-control @error('found') is-invalid @enderror"
                                    name="found" value="{{ old('found') }}" required autocomplete="found">
                                    <option value="">Please choose an option</option>
                                    <option value="Social Media">YouTube / Facebook Ads</option>
                                    <option value="YouTube">Found on YouTube</option>
                                    <option value="Referral">Referral</option>
                                    <option value="Web Search">Web Search (ie Google)</option>
                                </select>
                                @error('found')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password"
                                class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password"
                                    class="form-control @error('password') is-invalid @enderror" name="password"
                                    required autocomplete="new-password">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm"
                                class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control"
                                    name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-4"></div>
                            <div class="col-form-check col-md-6">
                                <input class="form-check-input" type="checkbox" name="newsletter"=id="newsletter">
                                <label class="form-check-label" for="newsletter">
                                    Sign up for our Newsletter!
                                </label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4"></div>
                            <div class="col-form-check col-md-6">
                                <input class="form-check-input" type="checkbox" name="terms" id="terms" required>
                                <label class="form-check-label" for="terms">
                                    I have read and agree to CKWraps <a href="terms-of-service" target="_blank">Terms of
                                        Service</a>
                                </label>
                            </div>
                            <div class="terms-error alert alert-danger col-md-6 offset-md-4 mt-3">
                            </div>
                        </div>
                        <div class="form-group row mb-3">
                            <div class="col-md-6 offset-md-4">
                                <div class="g-recaptcha" data-sitekey="6LevhcQUAAAAAOIg4xM5wBNzUgY6VV4oCMSeUXov">
                                </div>
                                <span role="alert" class="invalid-feedback d-block">
                                    <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                </span>
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary" id="submit-registration">
                                    {{ __('Continue to Payment') }}
                                </button>
                                <a class="btn btn-link" href="{{ route('login') }}">
                                    {{ __('Login') }}
                                </a>
                            </div>
                        </div>
                        <h2 class="mx-auto text-center mt-5 mb-3">Payment Options Available</h2>
                        <div class="d-flex justify-content-center" style="gap: 30px">
                          <div class="d-flex flex-column align-items-center">
                            <i class="fa fa-credit-card" style="font-size: 3rem"></i>
                            <p>Credit/Debit Card</p>
                          </div>
                          <div class="d-flex flex-column align-items-center">
                            <i class="fa fa-paypal" style="font-size: 3rem"></i>
                            <p>PayPal</p>
                          </div>
                        </div>
                        
                          <div class="alert alert-info offset-md-2 col-md-8">
                            <p class="mb-0">Please note, any coupons or promotions are only available via Credit Card payment. They cannot be applied to a PayPal payment.</p>
                          </div>
                    </form>
                </div>
            </div>
            <div class="col-md-12 text-center text-muted">
                <p>All prices are in US Dollars
                    <br>*Subscriptions are for a minimum of 3 months
                    <br>**We host our videos on Vimeo, some countries block access to Vimeo please check if your country
                    does this.
                </p>
            </div>
        </div>
    </div>
</div>
@endsection