@extends('layouts.app')
@section('extra-head')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.19.1/ui/trumbowyg.min.css"
  integrity="sha256-iS3knajmo8cvwnS0yrVDpNnCboUEwZMJ6mVBEW1VcSA=" crossorigin="anonymous" />
@endsection

@section('content')
<div class="container" style="margin-top:100px">
  <div class="row justify-content-center mb-4">
    <div class="col-lg-12">
      <h1 class="h2">Leave a Review</h1>
    </div>
  </div>
  <div class="row justify-content-center">
    @include('auth.account-sidebar')
    <div class="col-lg-8">
      @if (session('status'))
      <div class="alert alert-success">
        {{ session('status') }}
      </div>
      @endif
      <p>CKWraps allows its premium members to review or recommend our services. We may contact you regarding your review or publish your review to our testimonials page.</p>

      @if( $review !== null)
      {{Form::open(['action' => ['UserController@update_review'], 'method' => 'PUT'])}}
      @else 
      {{Form::open(['action' => ['UserController@create_review'], 'method' => 'POST'])}}
      @endif
      @csrf
      <div class="form-group">
        <label for="name">Your Name</label>
        <input type="text" class="form-control" name="name" id="name"
          value="@if(old('name')){{old('name')}}@else @if($review !== null){{ $review->name }}@endif @endif" required>
      </div>
      <div class="form-group">
        <label for="body">Your Review</label>
        <textarea rows="10" class="form-control" name="body" id="body"
           required>@if(old('review')){{old('review')}}@else @if($review !== null){{ $review->body }}@endif @endif</textarea>
      </div>
      @if( $review !== null )
      <button type="submit" class="btn btn-primary">Update</button>
      @else 
      <button type="submit" class="btn btn-primary">Create</button>
      @endif
      </form>


      @if ($errors->any())
      <div class="alert alert-danger mt-4">
        <ul style="margin:0">
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div><br />
      @endif

    </div>
  </div>
</div>
@endsection
