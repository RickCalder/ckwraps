@extends('layouts.app')

@section('content')
<div class="container" style="margin-top:100px">
  <div class="row justify-content-center mb-4">
    <div class="col-lg-12">
      <h1 class="h2">Subscription Details</h1>
    </div>
  </div>
  <div class="row justify-content-center">
    @include('auth.account-sidebar')
    <div class="col-lg-8">
      @if (session('status'))
      <div class="alert alert-success">
        {{ session('status') }}
      </div>
      @endif
      
      @if($subscription)
        <p>
          <strong>Plan:</strong> {{ $plan->name }}<br>
          <strong>Status:</strong> {{ $subscription->stripe_status }}<br>
          <strong>Activation Date:</strong> {{ $subscription->created_at->toDayDateTimeString() }}<br>
          <strong>Card Ending In:</strong> {{ $user->card_last_four }}<br>
          <strong>Subscription Status:</strong> {{ $subscription->stripe_status }}<br>
          @if($subscription->ends_at !== null)
          <strong>Subscription Ends:</strong> {{ $subscription->ends_at }}<br>
          @endif
        </p>
        <hr>
        <div>
          <h3 class="h5">Update Billing Address</h3>
          <form method="POST" action="{{route('update-address')}}">
            @csrf
            <div class="form-group">
              <label for="address-1">Address line 1</label><span style="color: red !important; display: inline; float: none;">*</span>
              <input value="{{$user->address1}}" type="text" class="form-control" id="address-1" name="address1" required>
              <label for="address-2">Address line 2</label>
              <input value="{{$user->address2}}"  type="text" class="form-control" id="address-2" name="address2">
              <label for="City">City</label><span style="color: red !important; display: inline; float: none;">*</span>
              <input value="{{$user->city}}"  type="text" class="form-control" id="city" name="city" required>
              <label for="state">State/Province</label>
              <input value="{{$user->state}}"  type="text" class="form-control" id="state" name="state">
              @include('partials.countries2')
              <label for="zip">Postal/Zip Code</label>
              <input value="{{$user->zip}}"  type="text" class="form-control" id="zip" name="zip">
            </div>
            <div class="form-group">
              <input type="submit" class="btn btn-primary" value="Update/Add Address">
            </div>
          </form>
          <div>
            <span style="color: red !important;">*</span> denotes required field
          </div>
        </div>
        <div>
          <h3 class="h5 mt-4">Update Payment Method</h3>
          @include('auth.payment_partial')
        </div>
        @if($subscription->ends_at === null)
        <hr>
        <h3 class="h5">
          Cancel Subscription
        </h3>
        @endif
        @php
        $subAge = now()->diffInDays( $subscription->created_at )
        @endphp
          @if($subAge < 75 ) <p>Your subscription is only {{$subAge}} days old, you may not cancel before 75 days.</p>

            @else
            @if($subscription->ends_at === null)
            <form method="POST" action="{{ route('subscription.cancel') }}">
              @csrf

              <div class="form-group">
                  <label for="cancel_reason">Reason for Cancelling (required)</label>
                  <textarea required id="cancel_reason"  class="form-control" name="cancel_reason"></textarea>
              </div>
              <button type="submit" class="btn btn-danger">Cancel Subscription</button>
            </form>
            @endif
          @endif
        @else
          @if(auth()->user()->role_id === 1)
            <p>You are an administrator, you do not require a subscription.</p>
            @elseif(auth()->user()->role_id === 3 && ( isset($subscription) && $subscription->ends_at !== null))
            Your subscription will expire on: {{ $subscription->ends_at->format('F jS, Y') }}

            @elseif(auth()->user()->role_id === 3)
            <p>You are currently registered as a free member, to subscribe as a premium member please <a
                href="/plans?upgrade=1">choose a plan.</a></p>
            <p>
              As a premium member you'll gain access to our full video course catalogue, forums, location board and the
              ability to add your business to our list of locations!
            </p>
            <p>
              <a href="/plans?upgrade=1" class="btn btn-primary btn-lg">Upgrade Now!</a>
            </p>
            @else
            <p>You are currently registered as a Premium Member, you can manage your subscription by logging into PayPal.</p>
            @endif
          @endif
        @if ($errors->any())
        <div class="alert alert-danger mt-4">
          <ul style="margin:0">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div><br />
        @endif

    </div>
  </div>
</div>
@endsection