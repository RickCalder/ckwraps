@extends('layouts.app')


@section('content')
<div class="container" style="margin-top: 100px">
  <div class="row">
    <div class="col-md-12">
      <h1 class="mb-5">Important Documents</h1>
    </div>
  </div>
  <div class="row mb-5">
    <div class="col-lg-12">
      @if(!$allowed)
      <p class="alert alert-danger">Important Documents are available for premium members only. <a href="/register">Join now!</a> or <a href="/login">login to your account.</a></p>
      @endif
      @if($downloads->first())
      @foreach ($downloads as $download)
        <h3>{{ $download->title }}</h3>
        <div class="d-flex mt-3">
          <p class="text-center mr-4">
            @if($allowed)
            <a href="{{Storage::url($download->file)}}" title="View {{$download->title}}" target="_blank" class="h6"><i class="fa fa-file-pdf-o" style="font-size: 4rem; color: orange"></i><br>Download</a>
            @else 
            <i class="fa fa-file-pdf-o" style="font-size: 4rem; color: orange"></i><br><span class="h6">Download</span>
            @endif
          </p>
          <p>
            {!! $download->description !!}
          </p>
        </div>
        
        <hr>
      @endforeach
      @else 
      <p class="h4">There are no files to download right now, please check back later!</p>
      @endif
  </div>
</div>
@endsection

