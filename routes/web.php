<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::post('/stripe/webhook', '\Laravel\Cashier\Http\Controllers\WebhookController@handleWebhook');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/advertise', 'HomeController@advertise')->name('advertise');
Route::get('/', 'HomeController@index')->name('home');
Route::get('/giveaway', 'HomeController@giveaway')->name('giveaway');
Route::get('/training', 'HomeController@training')->name('training');
Route::get('/ppf-training', 'HomeController@ppf_training')->name('ppf-training');
Route::get('/cosmetic-ppf', 'HomeController@cosmetic_ppf_training')->name('cosmetic-ppf-training');
Route::get('/terms-of-service', 'HomeController@tos')->name('tos');
Route::get('/privacy', 'HomeController@privacy')->name('privacy');
Route::get('/contact', 'ContactController@index')->name('contact');
Route::get('/contact-ppf', 'ContactController@contact_ppf')->name('contact-ppf');
Route::post('/contact-ppf', 'ContactController@mail_ppf');
Route::get('/contact-cppf', 'ContactController@contact_cppf')->name('contact-cppf');
Route::post('/contact-cppf', 'ContactController@mail_cppf');
Route::post('/contact', 'ContactController@store');
Route::get('/questions-answers', 'FaqController@index')->name('faq');
Route::get('/shop', 'ProductController@index')->name('shop');
Route::get('/resources', 'SponsorController@index')->name('resources');
Route::get('/locations', function() {return redirect('/');})->name('locations');
Route::get('/testimonials', 'ReviewController@index')->name('testimonials');
Route::get('/locations/{id}',  function() {return redirect('/');})->name('location');
Route::post('/paypal_cancel', 'PaypalController@subscriptionCancel');


Route::group(['middleware' => 'allowed'], function() {
    Route::get('/videos', 'VideoController@index')->name('videos');
    Route::get('/live-events', 'LiveEventController@index')->name('live-events');
    Route::get('/videos/cats/{cat}', 'VideoController@categories')->name('video-cats');
    Route::get('/videos/{id}', 'VideoController@show')->name('video');
    Route::get('/my-account', 'UserController@account')->name('account');
    Route::get('/my-account/subscription', 'UserController@subscription')->name('subscription');
    Route::post('/my-account/subscription', 'UserController@address')->name('update-address');
    Route::put('/my-account', 'UserController@update');
    Route::get('/my-account/location', 'UserController@account')->name('user_location');
    Route::put('/my-account/location', 'UserController@account');
    Route::post('/my-account/location', 'UserController@account');
    Route::get('/my-account/review', 'UserController@review')->name('user_review');
    Route::post('/my-account/review', 'UserController@create_review');
    Route::put('/my-account/review', 'UserController@update_review');
    Route::post('/vote', 'ReplyController@vote')->name('replies.vote');
    Route::any('/search', 'VideoController@search')->name('video-search');
    Route::get('/important-documents', 'DownloadsController@index')->name('downloads');
    Route::post('create-comment', 'VideoController@comment');
    Route::post('{id}/delete-comment', 'VideoController@comment_delete')->name('videocomment.delete');
    Route::get('{id}/edit-comment', 'VideoController@comment_edit')->name('videocomment.edit');
    Route::put('{reply}/update_comment', 'VideoController@update_comment')->name('videocomment.update_comment');
    Route::post('/my-acount/save_avatar', 'UserController@avatar')->name('avatar');
    Route::get('/generate_signature', 'UploadController@generate_signature');
});

Route::group(['prefix' =>'forums', 'middleware' => ['allowed']], function () {
    Route::post('/like', 'ForumController@like_thread')->name('threads.like');
    Route::post('/like_reply', 'ForumController@like_reply')->name('replies.like');
    Route::any('/search', 'ForumController@search')->name('forum-search');
    Route::get('/', 'ForumController@overview')->name('forums');
    Route::get('/my-threads', 'ForumController@my_threads')->name('forums-my-threads');
    Route::get('/my-replies', 'ForumController@my_replies')->name('forums-my-replies');
    Route::get('create-thread', 'ForumController@create')->name('threads.create');
    Route::post('create-thread', 'ForumController@store')->name('threads.store');
    Route::get('{thread}', 'ForumController@show')->name('thread');
    Route::post('{thread}', 'ReplyController@store');
    Route::delete('{thread}/delete', 'ForumController@delete_thread')->name('threads.delete');
    Route::get('user/{user_id}', 'ForumController@show_user');
    Route::get('{thread}/edit', 'ForumController@edit')->name('threads.edit');
    Route::put('{thread}', 'ForumController@update_thread')->name('threads.update');
    Route::get('{reply}/edit_reply', 'ReplyController@edit_reply')->name('replies.edit_reply');
    Route::put('{reply}/update_reply', 'ReplyController@update_reply')->name('replies.update');
    Route::delete('{reply}/delete_reply', 'ReplyController@delete_reply')->name('replies.delete');
    Route::get('tags/{tag}', 'ThreadTagController@show')->name('forums.tag');
});

//Subscriptions
Route::get('/plans', 'PlanController@index')->name('plans');

Route::group(['middleware' => 'auth'], function() {
    Route::get('/plans/{slug}', 'PlanController@show')->name('plan');
    Route::post('/subscription', 'SubscriptionController@create')->name('subscription.create');
    Route::post('/subscription-update', 'SubscriptionController@update_payment')->name('subscription.update_payment');
    Route::post('/subscription-cancel', 'SubscriptionController@cancel')->name('subscription.cancel');
    Route::get('/thank-you', 'PlanController@thanks')->name('thankyou');
    Route::get('/subscribe/paypal/return/', 'PaypalController@paypalReturn')->name('paypal.return');
    Route::get('/subscribe/paypal/{id_plan}', 'PaypalController@paypalRedirect')->name('paypal.redirect');
    // Route::get('/create_paypal_plan', 'PaypalController@create_plan');
});


//Clear configurations:
Route::get('/config-clear', function() {
    $status = Artisan::call('config:clear');
    return '<h1>Configurations cleared</h1>';
});

//Clear cache:
Route::get('/cache-clear', function() {
    $status = Artisan::call('cache:clear');
    return '<h1>Cache cleared</h1>';
});

Route::post('/mcsubscribe', 'HomeController@mcsubscribe')->name('mcsubscribe');

//Clear configuration cache:
Route::get('/config-cache', function() {
    $status = Artisan::call('config:cache');
    return '<h1>Configurations cache cleared</h1>';
});
